import { Collections } from "@/shop/catalog";
import Slider from "@/shop/components/slider";
import { Collection, Promotion } from "@/shop/product";
import type { Product } from "@/shop/product";
import { Samvydat, SamvydatCollection } from "@/shop/samvydat";
import { getProductsList } from "@/shopify";

type ShopProps = {
  promo: Product[];
  samvydat?: SamvydatCollection;
  collections: Collection[];
};

export const metadata = {
  title: "ByProsvet - самвыдат Беларусі",
};

export const dynamic = "force-dynamic";

export default async function Shop() {
  const { promo, samvydat, collections }: ShopProps =
    await getServerSideProps();

  return (
    <div>
      {promo?.length > 0 && (
        <Slider>
          {promo.map(item => (
            <Promotion key={item.id} product={item} />
          ))}
        </Slider>
      )}
      <Collections collections={collections} />
      {samvydat && <Samvydat collection={samvydat} />}
    </div>
  );
}

async function getServerSideProps() {
  const { promo, samvydat, collections } = await getProductsList();

  return {
    promo,
    samvydat,
    collections,
  };
}
