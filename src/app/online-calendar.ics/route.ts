export const GET = () =>
  Response.redirect(
    "https://calendar.google.com/calendar/ical/d8eb8394349e7799b2fb7787c19b50ce6a90ce3c489e8cb0962a00cba2f98cf4%40group.calendar.google.com/public/basic.ics",
    307,
  );
