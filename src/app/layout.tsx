"use client";

import type { PropsWithChildren } from "react";

import { links } from "@/uikit/links";
import { Global, css } from "@emotion/react";

export default function Document({ children }: PropsWithChildren) {
  return (
    <html>
      <head>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;700&display=swap"
          rel="stylesheet"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="shortcut icon" href="/favicon.ico" />
        <Global
          styles={css`
            body {
              margin: 0;
              padding: 0;
              font-family: "Montserrat", sans-serif;
            }
            ${links}
          `}
        />
      </head>
      <body>{children}</body>
    </html>
  );
}
