import { Suspense } from "react";

import { Cart } from "@/shop/cart";

export const metadata = {
  title: "Кошык - ByProsvet - самвыдат Беларусі",
};

export default function CartPage() {
  return (
    <div>
      <Suspense>
        <Cart />
      </Suspense>
    </div>
  );
}
