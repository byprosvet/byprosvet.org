import { Catalog } from "@/shop/catalog";
import { CollectionDescription } from "@/shop/collection";
import { getCollectionByHandle } from "@/shopify";
import { Error } from "@/uikit/error";

type Props = {
  params: Promise<{
    slug: string;
  }>;
};

export async function generateMetadata({ params }: Props) {
  const { slug } = await params;
  const { collection } = await getServerSideProps(decodeURIComponent(slug));
  const title = `${collection?.title} - ByProsvet`;

  return {
    title,
  };
}

export default async function CollectionPage({ params }: Props) {
  const { slug } = await params;
  const { collection } = await getServerSideProps(decodeURIComponent(slug));
  if (!collection) {
    return <Error />;
  }
  return (
    <>
      <CollectionDescription collection={collection} />
      <Catalog products={collection.products ?? []} />
    </>
  );
}

async function getServerSideProps(handle: string) {
  const collection = await getCollectionByHandle(handle).catch(() => null);
  return {
    collection,
  };
}
