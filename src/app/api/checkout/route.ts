import { getDeliveryMethods } from "@/shopify";
import { handleServerError } from "@/zod";

export const GET = handleServerError(async function handler(req: Request) {
  const reqUrl = new URL(req.url);
  const countryCode = reqUrl.searchParams.get("country_code") ?? "";
  const response = await getDeliveryMethods(countryCode);
  return Response.json(response);
});
