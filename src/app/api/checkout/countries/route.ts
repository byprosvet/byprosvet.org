import { getCountriesList } from "@/shopify";
import { handleServerError } from "@/zod";

export const dynamic = "force-dynamic";

export const GET = handleServerError(async function handler() {
  const response = await getCountriesList();
  return Response.json(response);
});
