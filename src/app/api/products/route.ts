import { getProductsList } from "@/shopify";
import { handleServerError } from "@/zod";

export const dynamic = "force-dynamic";

export const GET = handleServerError(async function handler() {
  const response = await getProductsList();
  return Response.json(response);
});
