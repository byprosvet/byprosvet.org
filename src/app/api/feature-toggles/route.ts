export const dynamic = "force-dynamic";

export const GET = async function () {
  return Response.json({
    isCartEnabled: process.env?.IS_CART_ENABLED === "1",
  });
};
