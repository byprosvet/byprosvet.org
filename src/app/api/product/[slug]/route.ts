import { getProductByHandle } from "@/shopify";
import { handleServerError } from "@/zod";

export const GET = handleServerError(async function (req: Request) {
  const reqUrl = new URL(req.url);
  const handle = reqUrl.searchParams.get("handle");
  const response = await getProductByHandle(handle || "");
  return Response.json(response);
});
