import { logger } from "@/logger";
import { Catalog } from "@/shop/catalog";
import { FullProduct } from "@/shop/product/full-view";
import { getProductByHandle } from "@/shopify";
import { Error } from "@/uikit/error";

type Props = {
  params: Promise<{ slug: string }>;
};

export async function generateMetadata({ params }: Props) {
  const { slug } = await params;
  const { product } = await getProducts(slug);
  const title = product
    ? `${product.title} - ByProsvet`
    : `Не знайшлі прадукт - ByProsvet`;

  return {
    title,
  };
}

export default async function ProductPage({ params }: Props) {
  const { slug } = await params;
  const { product, recommendations } = await getProducts(slug);
  if (!product) {
    logger.info({ message: "Product was not found", slug });
    return <Error />;
  }
  return (
    <>
      <FullProduct product={product} />
      <Catalog products={recommendations} />
    </>
  );
}

async function getProducts(handle: string) {
  const { product, recommendations } = await getProductByHandle(handle).catch(
    () => ({
      product: null,
      recommendations: [],
    }),
  );
  return {
    product,
    recommendations,
  };
}
