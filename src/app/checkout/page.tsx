import { Suspense } from "react";

import { Checkout } from "@/shop/checkout/info";

export const metadata = {
  title: "Афармленне заказу - ByProsvet - самвыдат Беларусі",
};

export default function CheckoutPage() {
  return (
    <Suspense>
      <Checkout />
    </Suspense>
  );
}
