import { Suspense } from "react";

import { Delivery } from "@/shop/checkout/delivery";

export const metadata = {
  title: "Спосабы дастаўкі - ByProsvet - самвыдат Беларусі",
};

export default function DeliveryPage() {
  return (
    <Suspense>
      <Delivery />
    </Suspense>
  );
}
