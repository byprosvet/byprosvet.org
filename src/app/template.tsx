"use client";

import { PropsWithChildren } from "react";

import { Footer } from "@/shop/footer";
import { Header } from "@/shop/navigation";
import styled from "@emotion/styled";

export default function Template({ children }: PropsWithChildren) {
  return (
    <ScreenView>
      <MainContainer>
        <Header />
        <main>{children}</main>
      </MainContainer>
      <Footer />
    </ScreenView>
  );
}

const ScreenView = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-height: 100vh;
`;
const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  a {
    text-decoration: none;
  }
`;
