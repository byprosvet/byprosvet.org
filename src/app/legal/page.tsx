import { TextContainer } from "@/uikit/text-container";

export const metadata = {
  title: "Пагадненні і тэрміны - ByProsvet - самвыдат Беларусі",
};

export default function LegalAndTerms() {
  return (
    <TextContainer>
      <h1>Палітыка прыватнасці</h1>
      <p>
        Пад час афармлення заказаў мы збіраем вашы асабістыя дадзеныя праз
        платформу <a href="https://www.shopify.com">Shopify</a>, падрабязнее пра
        тое, як мы збіраем і карыстаемся вашымі дадзенымі можна даведацца на{" "}
        <a href="https://www.shopify.com/legal/privacy">
          shopify.com/legal/privacy
        </a>{" "}
        і{" "}
        <a href="https://www.shopify.com/legal/privacy/app-users">
          shopify.com/legal/privacy/app-users
        </a>
        .
      </p>
      <h1>Палітыкі карыстання cookies</h1>
      <p>
        Функцыі анлайн-крамы заснаваны на магчымасцях платформы{" "}
        <a href="https://www.shopify.com">Shopify</a>, падрабязнее пра тое, якія
        cookies выкарыстоўваюцца пад час афармлення заказу можна даведацца на{" "}
        <a href="https://www.shopify.com/legal/cookies">
          shopify.com/legal/cookies
        </a>
        .
      </p>
    </TextContainer>
  );
}
