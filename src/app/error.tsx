"use client";

import { Suspense } from "react";

import { Error } from "@/uikit/error";

export default function ErrorPage() {
  return (
    <Suspense>
      <Error />
    </Suspense>
  );
}
