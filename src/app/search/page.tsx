import { Catalog, Collections } from "@/shop/catalog";
import { SearchPageBreadcrumbs } from "@/shop/search-breadcrumbs";
import { searchAllProducts, searchCollections } from "@/shopify/search";

type Props = {
  searchParams: Promise<{
    q?: string;
  }>;
};

export const metadata = {
  title: "Пошук - ByProsvet",
};

const SearchPage = async ({ searchParams }: Props) => {
  const { q } = await searchParams;
  const { collections, products, totalProductsCount } =
    await getServerSideProps(q ?? "");

  return (
    <div>
      <SearchPageBreadcrumbs itemsCount={totalProductsCount} />

      <Catalog products={products} />

      <Collections collections={collections} />
    </div>
  );
};

export default SearchPage;

const getServerSideProps = async (q: string) => {
  const [collections, { products, totalCount: totalProductsCount }] =
    await Promise.all([searchCollections(q), searchAllProducts(q)]);

  return {
    collections,
    products,
    totalProductsCount,
  };
};
