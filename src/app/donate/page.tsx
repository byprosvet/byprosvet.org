import { Suspense } from "react";

import { Donations } from "@/shop/donate";
import { TextContainer } from "@/uikit/text-container";

export default function Donate() {
  return (
    <Suspense>
      <TextContainer>
        <Donations />
      </TextContainer>
    </Suspense>
  );
}
