import gql from "graphql-tag";

export const collection = gql`
  fragment CollectionFragment on Collection {
    title
    handle
    description
  }
`;
