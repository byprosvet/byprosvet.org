import {
  type ShopifyCollection,
  flattenCollection,
} from "@/shopify/collection";
import { fetchFromShopify } from "@/shopify/fetch";
import { ProductNode, flattenProduct } from "@/shopify/products";
import { print } from "graphql";

import * as SearchSchema from "./search.graphql";

const MAX_COUNT_TO_FETCH = 250;

type SearchResponse = {
  data: {
    search: {
      edges: Array<{ node: ProductNode; cursor: number }>;
      totalCount: number;
    };
  };
};

export async function searchProducts(
  search: string,
  limit?: number,
  cursor?: string,
) {
  const response = await fetchFromShopify<SearchResponse>(
    print(SearchSchema.searchProducts),
    {
      query: search,
      first: limit,
      cursor,
    },
  );

  const { edges, totalCount } = response.data.search;
  return {
    products: edges.map((p: any) => flattenProduct(p.node)),
    lastProductCursor: edges[edges.length - 1]?.cursor,
    totalCount,
  };
}

export async function searchAllProducts(search: string) {
  const { products, totalCount } = await searchProducts(
    search,
    MAX_COUNT_TO_FETCH,
  );

  return {
    products,
    totalCount,
  };
}

export async function searchCollections(search: string, limit?: number) {
  const {
    data: {
      predictiveSearch: { collections },
    },
  } = await fetchFromShopify<any>(print(SearchSchema.suggestions), {
    query: search,
    limit,
  });

  return collections.map((c: ShopifyCollection) => flattenCollection(c));
}
