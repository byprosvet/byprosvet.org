import z from "zod";

import { fetchFromShopify } from "../fetch";

export const StartCheckoutPayload = z.object({
  cart: z.array(
    z.object({
      id: z.string(),
      quantity: z.number(),
    }),
  ),
  deliveryInfo: z.object({
    deliveryMethodId: z.string(),
    email: z.string(),
    firstName: z.string().optional(),
    lastName: z.string().optional(),
    phoneNumber: z.string().optional(),
    address: z.string().optional(),
    postalCode: z.string().optional(),
    city: z.string().optional(),
    country: z.string(),
    paczkomatNumber: z.string().optional(),
  }),
  coupon: z.string().optional(),
});

export type StartCheckoutPayloadType = z.infer<typeof StartCheckoutPayload>;

type CheckoutCreateResponse = {
  data: {
    checkoutCreate: {
      checkout: {
        id: string;
        webUrl: string;
        totalPrice: {
          amount: string;
          currencyCode: string;
        };
        subtotalPrice: {
          amount: string;
          currencyCode: string;
        };
        lineItems: {
          nodes: Array<{
            title: string;
            variant: {
              id: string;
              quantity: number;
              price: {
                amount: string;
                currencyCode: string;
              };
            };
          }>;
        };
      };
    };
  };
};
export async function startCheckout(payload: StartCheckoutPayloadType) {
  const { cart, deliveryInfo } = payload;
  const response = await fetchFromShopify<CheckoutCreateResponse>(
    `mutation checkoutCreate($input: CheckoutCreateInput!) {
    checkoutCreate(input: $input) {
      checkout {
        id
        webUrl
        totalPrice {
          amount
          currencyCode
        }
        subtotalPrice {
          amount
          currencyCode
        }
      }
    }
  }`,
    {
      input: {
        lineItems: cart.map(item => ({
          variantId: item.id,
          quantity: item.quantity,
        })),
        email: deliveryInfo.email,
        customAttributes: [
          {
            key: "Нумар пачкамата",
            value: deliveryInfo.paczkomatNumber,
          },
        ],
        shippingAddress: {
          address1: deliveryInfo.address ?? "Unknown",
          city: deliveryInfo.city ?? "Unknown",
          country: deliveryInfo.country,
          firstName: deliveryInfo.firstName,
          lastName: deliveryInfo.lastName,
          phone: deliveryInfo.phoneNumber ?? "000-000-000",
          zip: deliveryInfo.postalCode ?? "00000",
        },
      },
    },
  );

  const { checkout } = response?.data?.checkoutCreate;

  return {
    url: checkout?.webUrl,
    totalPrice: checkout?.totalPrice,
    subtotalPrice: checkout?.subtotalPrice,
  };
}
