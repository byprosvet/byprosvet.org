import { fetchFromAdmin } from "../fetch";

type DeliveryProfilesResponse = {
  data: {
    deliveryProfiles: {
      edges: Array<{
        node: {
          id: string;
          name: string;
          profileLocationGroups: Array<{
            locationGroupZones: {
              edges: Array<{
                node: {
                  zone: {
                    id: string;
                    name: string;
                    countries: Array<{
                      id: string;
                      name: string;
                      code: {
                        countryCode: string;
                        restOfWorld: boolean;
                      };
                    }>;
                  };
                  methodDefinitions: {
                    edges: Array<{
                      node: {
                        id: string;
                        name: string;
                        methodConditions: Array<{
                          id: string;
                          field: string;
                          operator: string;
                          conditionCriteria: {
                            unit: string;
                            value: number;
                          };
                        }>;
                        rateProvider: {
                          id: string;
                          price: {
                            amount: number;
                            currencyCode: string;
                          };
                        };
                      };
                    }>;
                  };
                };
              }>;
            };
          }>;
        };
      }>;
    };
  };
};

type Countries = Array<{
  name: string;
  countryCode: string;
}>;

type DeliveryMethod = {
  id: string;
  name: string;
};

type DeliveryProfileEdge =
  DeliveryProfilesResponse["data"]["deliveryProfiles"]["edges"][number];

type MethodDefinitionNode =
  DeliveryProfileEdge["node"]["profileLocationGroups"][number]["locationGroupZones"]["edges"][number]["node"]["methodDefinitions"]["edges"][number]["node"];

const parseMethodDefinitionType = (methodDefinition: MethodDefinitionNode) => {
  if (methodDefinition.name.includes("InPost - Paczkomat")) {
    return "inpost-paczkomat";
  }

  if (methodDefinition.name.includes("InPost - Dom lub firma")) {
    return "mail";
  }

  if (methodDefinition.name.includes("Mail")) {
    return "mail";
  }

  if (methodDefinition.name.includes("Самавываз")) {
    return "self-pickup";
  }

  return "";
};

const parseDeliveryProfileEdge = (edge: DeliveryProfileEdge) => ({
  name: edge.node.name,
  profileLocationGroups: edge.node.profileLocationGroups.map(
    profileLocationGroup => ({
      locationGroupZones: profileLocationGroup.locationGroupZones.edges.map(
        edge => ({
          name: edge.node.zone.name,
          countries: edge.node.zone.countries.map(country => ({
            countryCode: country.code.countryCode,
            name: country.name,
          })),
          methodDefinitions: edge.node.methodDefinitions.edges.map(edge => ({
            id: edge.node.id,
            name: edge.node.name,
            methodConditions: edge.node.methodConditions,
            rateProvider: edge.node.rateProvider,
            type: parseMethodDefinitionType(edge.node),
          })),
        }),
      ),
    }),
  ),
});

const getDeliveryProfiles = async () => {
  const response = await fetchFromAdmin<DeliveryProfilesResponse>(
    `{
  deliveryProfiles(first: 10) {
    edges {
        node {
            id
            name
            profileLocationGroups {
                locationGroupZones(first: 5) {
                    edges {
                        node {
                            zone {
                                id
                                name
                                countries {
                                    id
                                    code {
                                        countryCode
                                        restOfWorld
                                    }
                                    name
                                    translatedName
                                }
                            }
                            methodDefinitions(first: 15) {
                                edges {
                                    node {
                                        id
                                        name
                                        methodConditions {
                                            id
                                            field
                                            operator
                                            conditionCriteria {
                                                ... on MoneyV2 {
                                                    amount
                                                    currencyCode
                                                }
                                                ... on Weight {
                                                    unit
                                                    value
                                                }
                                            }
                                        }
                                        rateProvider {
                                            ... on DeliveryParticipant {
                                                id
                                            }
                                            ... on DeliveryRateDefinition {
                                                id
                                                price {
                                                    amount
                                                    currencyCode
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
  }
}`,
  );
  if (!response?.data) {
    throw new Error("Unexpected shopify response", {
      cause: JSON.stringify(response),
    });
  }

  const deliveryProfiles = response?.data.deliveryProfiles.edges.map(edge =>
    parseDeliveryProfileEdge(edge),
  );

  return deliveryProfiles;
};

export const getCountries: () => Promise<Countries> = async () => {
  const deliveryProfiles = await getDeliveryProfiles();

  return deliveryProfiles.reduce(
    (countries: Countries, deliveryProfile) =>
      deliveryProfile.profileLocationGroups.reduce(
        (countries: Countries, group) =>
          group.locationGroupZones.reduce(
            (countries: Countries, zone) => [...countries, ...zone.countries],
            countries,
          ),
        countries,
      ),
    [] as Countries,
  );
};

export const getDeliveryMethods = async (countryCode: string) => {
  const deliveryProfiles = await getDeliveryProfiles();

  const deliveryMethods = deliveryProfiles.reduce(
    (deliveryMethods, deliveryProfile) =>
      deliveryProfile.profileLocationGroups.reduce(
        (deliveryMethods, group) =>
          group.locationGroupZones.reduce(
            (deliveryMethods, zone) =>
              deliveryMethods.concat(
                !!zone.countries.find(
                  country => country.countryCode === countryCode,
                )
                  ? zone.methodDefinitions
                  : [],
              ),
            deliveryMethods,
          ),
        deliveryMethods,
      ),
    [] as DeliveryMethod[],
  );

  return {
    deliveryMethods,
  };
};
