import { z } from "zod";

import { getCountries } from "./checkout";
import { findCollectionWithProducts } from "./collection";
import { fetchProductsForFrontpage } from "./frontpage";
import { findProductByHandle, findProductById } from "./products";

export { handleZodError } from "./zod";
export { startCheckout, getDeliveryMethods } from "./checkout";

const Handle = z.string().regex(/^[a-zA-Zа-яА-Я0-9іІўЎ\-]+$/);
const Id = z.string().regex(/^[0-9]+$/);

export async function getProductsList() {
  if (process.env.API_PROXY_URL) {
    return fetchFromProxy("products");
  }
  return fetchProductsForFrontpage();
}

export async function getCollectionByHandle(handle: string) {
  if (process.env.API_PROXY_URL) {
    return fetchFromProxy(`collection/${handle}`);
  }
  return findCollectionWithProducts(Handle.parse(handle), 50);
}

export async function getProductByHandle(handle: string) {
  if (process.env.API_PROXY_URL) {
    return fetchFromProxy(`product/${handle}`);
  }
  if (Id.safeParse(handle).success) {
    return findProductById(`gid://shopify/Product/${handle}`);
  }
  return findProductByHandle(Handle.parse(handle));
}

async function fetchFromProxy(path: string) {
  const proxyResponse = await fetch(
    `https://${process.env.API_PROXY_URL}/api/${path}`,
  );
  const json = await proxyResponse.json();
  return json;
}

export async function getCountriesList() {
  return {
    countries: await getCountries(),
  };
}
