const apiHost = process.env.SHOPIFY_API_HOST;
const storefrontAccessToken = process.env.SHOPIFY_STOREFRONT_ACCESS_TOKEN ?? "";
const adminApiHost = process.env.SHOPIFY_ADMIN_API_HOST;
const adminAccessToken = process.env.SHOPIFY_ADMIN_ACCESS_TOKEN ?? "";

export const fetchFromShopify = async <T = unknown>(
  graphqlQuery: string,
  variables?: unknown,
): Promise<T> => {
  const response = await fetch(`https://${apiHost}/api/2024-01/graphql.json`, {
    method: "post",
    headers: {
      "X-Shopify-Storefront-Access-Token": storefrontAccessToken,
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({
      query: graphqlQuery,
      variables,
    }),
  });
  const responseBody = await response.json();

  if (!responseBody.data || !!responseBody.error) {
    console.log(responseBody);
  }

  return responseBody;
};

export const fetchFromAdmin = async <T = unknown>(
  body: string,
  variables?: unknown,
): Promise<T> => {
  const response = await fetch(
    `https://${adminApiHost}/admin/api/2024-01/graphql.json`,
    {
      method: "post",
      headers: {
        "X-Shopify-Access-Token": adminAccessToken,
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify({
        query: body,
        variables,
      }),
    },
  );
  const responseBody = await response.json();

  if (!responseBody.data || !!responseBody.error) {
    console.log(responseBody);
  }

  return responseBody;
};
