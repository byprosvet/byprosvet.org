import gql from "graphql-tag";

export const product = gql`
  fragment ProductPreviewFragment on Product {
    id
    handle
    title
    tags
    descriptionHtml
    featuredImage {
      url
      height
      width
    }
    images(first: 10) {
      edges {
        node {
          url
          height
          width
        }
      }
    }
    priceRange {
      minVariantPrice {
        amount
        currencyCode
      }
    }
    compareAtPriceRange {
      maxVariantPrice {
        amount
        currencyCode
      }
    }
    variants(first: 10) {
      edges {
        node {
          id
        }
      }
    }
    metafields(identifiers: [{ namespace: "product", key: "redirect" }]) {
      value
    }
  }
`;
