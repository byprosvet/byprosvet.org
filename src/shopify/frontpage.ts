import { fetchFromShopify } from "./fetch";
import { ProductsResponse, flattenProduct, getProductsQuery } from "./products";
import {
  CollectionNode,
  findLatestNewspapersQuery,
  flattenCollection,
} from "./samvydat";

type Response = {
  catalog: ProductsResponse;
  promo: ProductsResponse;
  samvydat: CollectionNode;
};

export async function fetchProductsForFrontpage() {
  const r = await fetchFromShopify<{ data: Response }>(`{
    promo: ${getProductsQuery(`first:5,query:"tag:Promo"`)}
    samvydat: ${findLatestNewspapersQuery(2)}
  }`);
  const { promo, samvydat } = r.data;
  const promoCatalog = takeRandom(promo.edges.map(p => flattenProduct(p.node)));
  return {
    promo: promoCatalog,
    samvydat: flattenCollection(samvydat),
    collections: await getFrontpageCollections(),
  };
}

function takeRandom<T>(list: T[]): T[] {
  if (!list.length) return list;
  const randomElementIndex = Math.floor(Math.random() * list.length);
  const element = list[randomElementIndex];
  const listWithoutElement = list.filter(
    (_, index) => index !== randomElementIndex,
  );
  return [element, ...listWithoutElement];
}

type CollectionBase = {
  id: string;
  title: string;
  handle: string;
  frontPagePosition: number;
};
async function getFrontpageCollections() {
  const response = await fetchFromShopify<{
    data: {
      collections: {
        edges: Array<{
          node: {
            id: string;
            title: string;
            handle: string;
            frontPagePosition?: {
              value: string | null;
            };
          };
        }>;
      };
    };
  }>(`{
    collections(first: 50) {
        edges {
            node {
                id,
                title,
                handle,
                frontPagePosition: metafield(namespace:"byprosvet", key: "frontPagePosition") {
                    value
                }
            }
        }
    }
  }`);
  const collectionsOrdered = response.data.collections.edges
    .map(({ node }) => ({
      id: node.id,
      title: node.title,
      handle: node.handle,
      frontPagePosition: Number(node.frontPagePosition?.value) || null,
    }))
    .filter((node): node is CollectionBase => node.frontPagePosition !== null);

  const query = collectionsOrdered
    .map(node => `(title:${node.title})`)
    .join(" OR ");

  const shopifyQuery = `query getCollectionsForFrontPage($query: String!) {
    collections(first: 20,query:$query) {
      edges {
        node {
          id,
          title,
          handle,
          products(first: 5) {
            edges {
              node {
                id
                handle
                title
                tags
                descriptionHtml
                featuredImage {
                  url
                  height
                  width
                }
                priceRange {
                  minVariantPrice {
                    amount
                    currencyCode
                  }
                }
                compareAtPriceRange {
                  maxVariantPrice {
                    amount
                    currencyCode
                  }
                }
                variants(first: 10) {
                  edges {
                    node {
                      id
                    }
                  }
                }
                metafields(identifiers: [{namespace: "product", key: "redirect"}]) {
                  key
                  value
                }
              }
            }
          }
        }
      }
    }
  }`;

  const r = await fetchFromShopify<{
    data: {
      collections: {
        edges: Array<{
          node: {
            id: string;
            title: string;
            handle: string;
            products: ProductsResponse;
          };
        }>;
      };
    };
  }>(shopifyQuery, { query });

  const collections = r.data?.collections?.edges.map(({ node }) => ({
    title: node.title,
    handle: node.handle,
    frontPagePosition:
      collectionsOrdered.find(c => c.id === node.id)?.frontPagePosition ?? 0,
    products: node.products.edges.map(({ node }) => flattenProduct(node)),
  }));
  collections.sort((c1, c2) => c2.frontPagePosition - c1.frontPagePosition);
  return collections;
}
