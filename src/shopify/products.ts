import type { Product, VariantPrice } from "@/shop/product/model";

import { fetchFromShopify } from "./fetch";

export type Money = VariantPrice;
export type ProductNode = {
  id: string;
  title: string;
  descriptionHtml: string;
  featuredImage: {
    url: string;
    height: number;
    width: number;
  };
  images?: {
    edges: Array<{
      node: {
        url: string;
        height: number;
        width: number;
      };
    }>;
  };
  priceRange: {
    minVariantPrice: Money;
    maxVariantPrice: Money;
  };
  compareAtPriceRange: {
    minVariantPrice: Money;
    maxVariantPrice: Money;
  };
  variants?: {
    edges: Array<{
      node: {
        id: string;
      };
    }>;
  };
  metafields?: Array<{
    namespace: string;
    key: string;
    value: string;
  }>;
};
export type ProductsResponse = {
  edges: Array<{
    node: ProductNode;
  }>;
};
type Response = {
  data: {
    products: ProductsResponse;
  };
};

export const flattenProduct = (product: ProductNode): Product => ({
  ...product,
  gid: product.id,
  id: product.id.split("/").pop()!,
  variants: product.variants?.edges.map(variant => variant.node),
  images: product.images?.edges.map(image => image.node) ?? [],
  redirect: product.metafields?.find(metafield => metafield?.key === "redirect")
    ?.value,
});

export const flattenProductResponse = (response: Response) =>
  response.data.products.edges.map(p => flattenProduct(p.node));

export const productPreviewSelector = `
        id
        handle
        title
        tags
        descriptionHtml
        featuredImage {
          url
          height
          width
        }
        priceRange {
          maxVariantPrice {
            amount
            currencyCode
          }
          minVariantPrice {
            amount
            currencyCode
          }
        }
        compareAtPriceRange {
          maxVariantPrice {
            amount
            currencyCode
          }
        }
        variants(first: 10) {
          edges {
            node {
              id
            }
          }
        }
        metafields(identifiers: [{namespace: "product", key: "redirect"}]) {
          key
          value
        }
      `;

export const getProductsQuery = (productQuery: string) => {
  return `products(${productQuery}) {
    edges {
      node {
        ${productPreviewSelector}
      }
    }
  }`;
};

export async function findProductByHandle(handle: string) {
  const productIdResponse = await fetchFromShopify<{
    data?: { product: { id: string } };
  }>(`{
    product(handle:"${handle}") {
      id
    }
  }`);
  const gid = productIdResponse.data?.product?.id;
  if (!gid)
    return {
      product: null,
      recommendations: [],
    };
  return findProductById(gid);
}

export async function findProductById(gid: string) {
  const productId = gid;
  const response = await fetchFromShopify<{
    data?: { product?: ProductNode; productRecommendations: ProductNode[] };
  }>(
    `{
      product(id:"${productId}") {
        id
        handle
        title
        tags
        descriptionHtml
        featuredImage {
          url
          height
          width
        }
        images(first:10) {
          edges {
            node {
              url
              height
              width
            }
          }
        }
        priceRange {
          minVariantPrice {
            amount
            currencyCode
          }
        }
        compareAtPriceRange {
          maxVariantPrice {
            amount
            currencyCode
          }
        }
        variants(first: 10) {
          edges {
            node {
              id
            }
          }
        }
        metafields(identifiers: [{namespace: "product", key: "redirect"}]) {
          namespace
          key
          value
        }
      }
      productRecommendations(productId:"${productId}") {
        id
        handle
        title
        tags
        descriptionHtml
        featuredImage {
          url
          height
          width
        }
        priceRange {
          minVariantPrice {
            amount
            currencyCode
          }
        }
        compareAtPriceRange {
          maxVariantPrice {
            amount
            currencyCode
          }
        }
        variants(first: 10) {
          edges {
            node {
              id
            }
          }
        }
        metafields(identifiers: [{namespace: "product", key: "redirect"}]) {
          namespace
          key
          value
        }
      }
}`,
  );
  if (!response.data?.product) {
    return {
      product: null,
      recommendations: [],
    };
  }
  return {
    product: flattenProduct(response.data.product),
    recommendations: response.data.productRecommendations.map(flattenProduct),
  };
}
