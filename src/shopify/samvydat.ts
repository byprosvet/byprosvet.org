import { fetchFromShopify } from "./fetch";
import { ProductNode, flattenProduct } from "./products";

export type CollectionsResponse = {
  data: {
    collection: CollectionNode;
  };
};
export type CollectionNode = {
  title: string;
  description: string;
  products: {
    edges: Array<{ node: ProductNode }>;
  };
};

export const findLatestNewspapersQuery = (
  limit: number,
) => `collection(id:"gid://shopify/Collection/597029617991") {
  title
  description
  products(first:${limit}, sortKey:CREATED, reverse: true) {
    edges {
      node {
        id
        title
        descriptionHtml
        featuredImage {
          url
          height
          width
        }
        publishedAt: metafield(namespace: "byprosvet", key: "samvydatNewspaperPublishedAt") {
          value
        }
        newspaperUrl: metafield(namespace: "byprosvet", key: "samvydatNewspaperUrl") {
          value
        }
      }
    }
  }
}`;

export const flattenCollection = (collection: CollectionNode | null) => {
  return collection
    ? {
        ...collection,
        products: collection.products.edges.map(p => flattenProduct(p.node)),
      }
    : null;
};

export async function findLatestNewspapers(limit: number) {
  const response = await fetchFromShopify<CollectionsResponse>(`{
    ${findLatestNewspapersQuery(limit)}
  }`);
  if (!response.data) {
    return response;
  }
  return flattenCollection(response.data.collection);
}
