import { NextApiRequest, NextApiResponse } from "next";

import { ZodError } from "zod";

export const handleZodError =
  (handler: (req: NextApiRequest, res: NextApiResponse) => unknown) =>
  async (req: NextApiRequest, res: NextApiResponse) => {
    try {
      return await handler(req, res);
    } catch (e) {
      if (e instanceof ZodError) {
        return res.status(400).end();
      }
      throw e;
    }
  };
