import gql from "graphql-tag";

import { collection } from "./collection.fragment.graphql";
import { product } from "./product.fragment.graphql";

export const suggestions = gql`
  query suggestions($query: String!, $limit: Int) {
    predictiveSearch(
      query: $query
      limit: $limit
      limitScope: EACH
      unavailableProducts: HIDE
      types: COLLECTION
    ) {
      collections {
        ...CollectionFragment
        products(first: 10, reverse: true) {
          nodes {
            ...ProductPreviewFragment
          }
        }
      }
    }
  }
  ${product}
  ${collection}
`;

export const searchProducts = gql`
  query searchProducts($query: String!, $first: Int = 10, $cursor: String) {
    search(
      query: $query
      first: $first
      after: $cursor
      types: PRODUCT
      unavailableProducts: HIDE
    ) {
      edges {
        cursor
        node {
          ...ProductPreviewFragment
        }
      }
      totalCount
    }
  }
  ${product}
`;
