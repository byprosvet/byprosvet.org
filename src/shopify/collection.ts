import { fetchFromShopify } from "./fetch";
import { ProductNode, flattenProduct } from "./products";

type CollectionsResponse = {
  data: {
    collection: {
      title: string;
      handle: string;
      description: string;
      products: {
        edges: Array<{ node: ProductNode }>;
      };
    };
  };
};

export type ShopifyCollection = {
  products?: {
    nodes?: ProductNode[];
  };
  title: string;
  handle: string;
  description: string;
  image?: {
    url: string;
    height: string;
    width: string;
  };
};

export const flattenCollection = (collection: ShopifyCollection) => {
  return {
    ...collection,
    products: collection.products?.nodes?.map(p => flattenProduct(p)),
  };
};

export async function findCollectionWithProducts(
  handle: string,
  limit: number,
) {
  const response = await fetchFromShopify<CollectionsResponse>(
    `query getCollectionsWithProducts($handle: String!, $limit: Int!) {
    collection(handle: $handle) {
      title
      handle
      image {
        url
        height
        width
      }
      description
      products(first: $limit, reverse: true) {
        edges {
          node {
            id
            title
            handle
            descriptionHtml
            featuredImage {
              url
              height
              width
            }
            priceRange {
              minVariantPrice {
                amount
                currencyCode
              }
            }
            compareAtPriceRange {
              maxVariantPrice {
                amount
                currencyCode
              }
            }
            variants(first: 10) {
              edges {
                node {
                  id
                }
              }
            }
            metafields(identifiers: [{namespace: "product", key: "redirect"}]) {
              key
              value
            }
          }
        }
      }
    }
  }`,
    { handle, limit },
  );
  if (!response.data) {
    return response;
  }
  const collection = {
    ...response.data.collection,
    products: response.data.collection.products.edges.map(p =>
      flattenProduct(p.node),
    ),
  };
  return collection;
}
