import { SyntheticEvent, useEffect } from "react";

export const useOnClickOutside = (
  ref: React.MutableRefObject<HTMLElement | undefined>[],
  openState: boolean,
  handler: (e: SyntheticEvent) => void,
  dependencies: any[] = [],
) => {
  const arrayedRefs = Array.isArray(ref) ? ref : [ref];

  const listener = (e: any) => {
    const isClose =
      arrayedRefs[0].current &&
      arrayedRefs.filter(rr => rr.current?.contains(e.target)).length === 0;
    if (isClose) {
      handler(e);
    }
  };

  useEffect(() => {
    if (openState) {
      document.addEventListener("mousedown", listener);
      document.addEventListener("touchstart", listener);
    }
    return () => {
      document.removeEventListener("mousedown", listener);
      document.removeEventListener("touchstart", listener);
    };
  }, [openState, ...dependencies]);
};
