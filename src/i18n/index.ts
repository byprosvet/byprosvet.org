import { translation as bel } from "./bel";

export type Key = keyof typeof bel;

export const t = (key: Key) => {
  return bel[key] || key;
};
