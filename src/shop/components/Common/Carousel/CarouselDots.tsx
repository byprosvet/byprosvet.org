import { css } from "@emotion/react";
import styled from "@emotion/styled";

type Props = {
  count: number;
  activeIndex: number;
  onClick: (index: number) => void;
};

export const CarouselDots = ({ count, activeIndex, onClick }: Props) => {
  return (
    <DotContainer className="carousel-dots">
      {new Array(count).fill("").map((_, i) => {
        return (
          <Dot
            key={`dot-${i}`}
            isActive={activeIndex === i}
            onClick={() => onClick(i)}
          />
        );
      })}
    </DotContainer>
  );
};

const DotContainer = styled.div`
  position: absolute;
  display: flex;
  bottom: 1rem;
  left: 50%;
  transform: translateX(-50%);
`;

const Dot = styled.div<{ isActive: boolean }>(({ isActive }) => {
  return css`
    padding: 0.7rem;
    cursor: pointer;
    &:after {
      content: "";
      width: 8px;
      height: 8px;
      display: flex;
      border: 1px solid black;
      background-color: ${isActive ? "black" : "transparent"};
      border-radius: 50%;
    }
  `;
});
