import { mobileAndDown } from "@/uikit/breakpoints";
import { css } from "@emotion/react";
import styled from "@emotion/styled";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";

type Props = {
  direction: "left" | "right";
  onClick: (direction: Props["direction"]) => void;
};

export const CarouselArrow = ({ direction, onClick }: Props) => {
  const handleClick = (e: any) => {
    e.preventDefault();
    e.stopPropagation();
    onClick(direction);
  };

  return (
    <Arrow
      direction={direction}
      className="carousel-arrow"
      onClick={handleClick}
    >
      <ArrowBackIosIcon />
    </Arrow>
  );
};

const Arrow = styled.div<{ direction: Props["direction"] }>(({ direction }) => {
  const isLeft = direction === "left";
  return css`
    ${mobileAndDown`
      display: none;
    `}
    display: flex;
    align-items: center;
    justify-content: center;
    background-color: rgba(255, 255, 255, 0.5);
    border-radius: 50%;
    width: 50px;
    height: 50px;
    position: absolute;
    top: 50%;
    transform: translateY(-50%) translateX(${isLeft ? "50%" : "-50%"})
      rotate(${!isLeft ? "180deg" : "0deg"});
    left: ${isLeft ? 0 : "initial"};
    right: ${!isLeft ? 0 : "initial"};
    z-index: 10;
    cursor: pointer;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
    svg {
      margin-left: 8px;
    }
  `;
});
