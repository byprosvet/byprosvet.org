import { useCallback, useEffect, useRef, useState } from "react";

import { CarouselArrow } from "@/shop/components/Common/Carousel/CarouselArrow";
import { CarouselDots } from "@/shop/components/Common/Carousel/CarouselDots";
import { MobileHide, mobileAndDown } from "@/uikit/breakpoints";
import { containerPadding } from "@/uikit/layout";
import { SerializedStyles, css } from "@emotion/react";
import styled from "@emotion/styled";

type Props<T> = {
  data: T[];
  renderItem: (item: T, i: number) => JSX.Element;
  containerStyles?: SerializedStyles;
};

export const Carousel = <T = any,>({
  data,
  renderItem,
  containerStyles,
}: Props<T>) => {
  const sliderRef = useRef<HTMLDivElement>(null);
  const itemWidth = useRef<number>(0);
  const maxScroll = useRef(0);
  const currentScroll = useRef(0);

  const [state, setState] = useState({
    dotIndex: 0,
    dotsCount: 0,
    itemsInContainer: 0,
    areArrowsShown: false,
  });

  const onChangeDotIndex = (dotIndex: number) => {
    const slideWidth = state.itemsInContainer * dotIndex * itemWidth.current;
    setState(prev => ({ ...prev, dotIndex }));

    slideCarousel(slideWidth);
  };

  const slideCarousel = (scrollWidth: number) => {
    if (!sliderRef.current?.children?.length) return;

    const scrolledItems = Math.floor(scrollWidth / itemWidth.current);
    let actualScroll = scrollWidth;

    if (maxScroll.current - actualScroll < itemWidth.current) {
      actualScroll = maxScroll.current;
    } else if (actualScroll < itemWidth.current) {
      actualScroll = 0;
    }

    sliderRef.current?.scrollTo({
      left: actualScroll,
      top: 0,
      behavior: "smooth",
    });

    currentScroll.current = actualScroll;

    let dotIndex = Math.floor(scrolledItems / state.itemsInContainer);
    if (actualScroll === maxScroll.current) {
      dotIndex = state.dotsCount - 1;
    } else if (actualScroll === 0) {
      dotIndex = 0;
    }

    setState(prev => ({
      ...prev,
      dotIndex,
    }));
  };

  const onArrowClick = (direction: "left" | "right") => {
    if (!sliderRef.current?.children?.length) return;

    const scrollShift = itemWidth.current || 0;

    let actualScroll = sliderRef.current.scrollLeft;

    if (direction === "left") {
      actualScroll = Math.max(0, currentScroll.current - scrollShift);
    } else {
      actualScroll = Math.min(
        maxScroll.current,
        currentScroll.current + scrollShift,
      );
    }

    slideCarousel(actualScroll);
  };

  const initialCalculate = useCallback(() => {
    calculateWidth();
    setTimeout(calculateDotsCount);
  }, []);

  const calculateDotsCount = () => {
    if (!sliderRef.current?.children?.length) return;

    const itemsInContainer =
      Math.floor(sliderRef.current.offsetWidth / itemWidth.current) || 1;
    const dotsCount =
      Math.ceil(sliderRef.current.children.length / itemsInContainer) || 1;

    setState(prev => ({
      ...prev,
      dotsCount,
      itemsInContainer,
      areArrowsShown: data.length > itemsInContainer,
    }));
  };

  const calculateWidth = () => {
    if (!sliderRef.current?.children?.length) return;

    const children = sliderRef.current.children;

    itemWidth.current = children[0].clientWidth;
    maxScroll.current =
      -sliderRef.current?.clientWidth + children.length * itemWidth.current;
  };

  useEffect(() => {
    initialCalculate();
  }, [data]);

  useEffect(() => {
    window.addEventListener("resize", initialCalculate);

    return () => {
      window.removeEventListener("resize", initialCalculate);
    };
  }, []);

  return (
    <CarouselContainer>
      <CarouselWrapper ref={sliderRef} style={containerStyles}>
        {data.map((item, i) => {
          return renderItem(item, i);
        })}
      </CarouselWrapper>
      {state.areArrowsShown && (
        <>
          <CarouselArrow direction="left" onClick={onArrowClick} />
          <CarouselArrow direction="right" onClick={onArrowClick} />
        </>
      )}

      {state.dotsCount > 1 && (
        <MobileHide>
          <CarouselDots
            count={state.dotsCount}
            activeIndex={state.dotIndex}
            onClick={onChangeDotIndex}
          />
        </MobileHide>
      )}
    </CarouselContainer>
  );
};

const CarouselContainer = styled.div`
  ${containerPadding}
  display: flex;
  flex-direction: row;
  position: relative;
  max-width: 100vw;
  overflow: hidden;
  box-sizing: border-box;
  ${mobileAndDown`
    padding-right: 0;
  `}
`;

const CarouselWrapper = styled.div<{ style?: SerializedStyles }>(props => {
  return css`
    ${props.style}
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: space-between;
    padding-bottom: 4rem;
    padding-top: 2.5rem;
    background-color: #fff;
    overflow-x: scroll;
    box-sizing: border-box;
    &::-webkit-scrollbar {
      display: none;
    }
    -ms-overflow-style: none; /* IE and Edge */
    scrollbar-width: none; /* Firefox */
  `;
});
