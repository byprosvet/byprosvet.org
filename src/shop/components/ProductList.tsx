import Image from "next/image";

import styled from "@emotion/styled";
import { Box, Typography } from "@mui/material";

import { useCart } from "../cart";
import { getMinPrice } from "../product/model";

export const ProductList = () => {
  const cart = useCart();

  return (
    <Container>
      <ItemsContainer>
        {cart.items.map(item => (
          <Box
            display="flex"
            alignItems="flex-start"
            width={"100%"}
            key={item.product.id}
          >
            <Image
              src={item.product.featuredImage!.url}
              width={84}
              height={120}
              alt={item.product.title}
              style={{
                marginRight: "1rem",
              }}
            />
            <Typography
              color="textPrimary"
              variant="subtitle1"
              style={{
                fontSize: "0.875rem",
                fontFamily: "Montserrat",
                fontWeight: 500,
                lineHeight: "1.25rem",
                flexGrow: 1,
              }}
            >
              {item.product.title}
            </Typography>
            <Typography
              color="textPrimary"
              variant="subtitle1"
              style={{
                fontSize: "0.875rem",
                fontFamily: "Montserrat",
                fontWeight: 500,
                lineHeight: "1.125rem",
              }}
            >
              {getMinPrice(item.product)}zl
            </Typography>
          </Box>
        ))}
      </ItemsContainer>
      <TotalContainer>
        <Box width={"100%"}>
          <Box
            display="flex"
            alignItems="flex-start"
            width={"100%"}
            justifyContent="space-between"
          >
            <Typography
              color="textPrimary"
              variant="subtitle1"
              style={{
                fontSize: "0.875rem",
                fontFamily: "Montserrat",
                fontWeight: 500,
                lineHeight: "1.25rem",
                flexGrow: 1,
              }}
            >
              Промежуточный итог
            </Typography>
            <Typography
              color="textPrimary"
              variant="subtitle1"
              style={{
                fontSize: "0.875rem",
                fontFamily: "Montserrat",
                fontWeight: 500,
                lineHeight: "1.125rem",
              }}
            >
              {cart.totalCost}zl
            </Typography>
          </Box>
          <Box
            display="flex"
            alignItems="flex-start"
            width={"100%"}
            justifyContent="space-between"
          >
            <Typography
              color="textPrimary"
              variant="subtitle1"
              style={{
                fontSize: "0.875rem",
                fontFamily: "Montserrat",
                fontWeight: 500,
                lineHeight: "1.25rem",
                flexGrow: 1,
              }}
            >
              Доставка
            </Typography>
            <Typography
              color="#979797"
              variant="subtitle1"
              style={{
                fontSize: "0.875rem",
                fontFamily: "Montserrat",
                fontWeight: 500,
                lineHeight: "1.125rem",
              }}
            ></Typography>
          </Box>
        </Box>
        <Box
          display="flex"
          alignItems="flex-start"
          width={"100%"}
          justifyContent="space-between"
        >
          <Typography
            color="textPrimary"
            variant="subtitle1"
            style={{
              fontSize: "1.125rem",
              fontFamily: "Montserrat",
              fontWeight: 500,
              lineHeight: "1.25rem",
              flexGrow: 1,
            }}
          >
            Общая сумма
          </Typography>
          <Typography
            color="textPrimary"
            variant="subtitle1"
            style={{
              fontSize: "1.125rem",
              fontFamily: "Montserrat",
              fontWeight: 500,
              lineHeight: "1.125rem",
            }}
          >
            {Number(cart.totalCost)}
            zl
          </Typography>
        </Box>
      </TotalContainer>
    </Container>
  );
};

const Container = styled.div`
  background: var(--background-grey, #f8f7f5);
  display: flex;
  padding: 2.5rem;
  flex-direction: column;
  align-items: flex-start;
  gap: 2rem;
  flex-shrink: 0;
  align-self: stretch;
  min-width: 32.5rem;
  height: 100%;
  overflow: auto;
  box-sizing: border-box;
`;

const ItemsContainer = styled.div`
  overflow: auto;
  flex-grow: 1;
  gap: 1rem;
  display: flex;
  padding-right: 1rem;
  flex-direction: column;
`;

const TotalContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  gap: 1rem;
  flex-shrink: 0;
  align-self: stretch;
  position: sticky;
  bottom: 0;
  background: var(--background-grey, #f8f7f5);
  padding: 2.5rem;
`;
