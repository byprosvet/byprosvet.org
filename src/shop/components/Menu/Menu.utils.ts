import { Key, t } from "@/i18n";

export type MenuItems = "categories.label" | "search.label";

const MENU_ITEMS: {
  name: MenuItems;
  path?: string[];
}[] = [
  {
    name: "categories.label",
  },
  {
    name: "search.label",
    path: ["/"],
  },
];

export const getMenuItems = (pathname: string) => {
  return MENU_ITEMS.filter(item => {
    return item.path ? item.path.includes(pathname) : true;
  }).map(item => {
    return {
      ...item,
      label: t(item.name as Key),
    };
  });
};
