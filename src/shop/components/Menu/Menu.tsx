import { useEffect, useState } from "react";

import { useRouter } from "next/router";

import { MenuItems, getMenuItems } from "@/shop/components/Menu/Menu.utils";
import { MenuCategories } from "@/shop/components/Menu/components/MenuCategories/MenuCategories";
import { MenuSearch } from "@/shop/components/Menu/components/MenuSearch/MenuSearch";
import { Collection } from "@/shop/product";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";

import styles from "./Menu.module.scss";

type Props = {
  collections: Collection[];
};

export const Menu = ({ collections }: Props) => {
  const router = useRouter();
  const menuItems = getMenuItems(router.pathname);

  const [tab, setTab] = useState<MenuItems>(menuItems[0].name);

  const handleChange = (e: React.SyntheticEvent, value: MenuItems) => {
    setTab(value);
  };

  useEffect(() => {
    const currentTab = menuItems.find(item => item.name === tab)?.name;
    if (!currentTab) {
      setTab(menuItems[0].name);
    }
  }, [router.pathname]);

  return (
    <div>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs value={tab} onChange={handleChange} aria-label="menu">
          {menuItems.map(({ label, name }) => {
            return <Tab label={label} key={name} value={name} />;
          })}
        </Tabs>
      </Box>
      <Box className={styles.tabContainer}>
        {tab === "categories.label" && (
          <MenuCategories collections={collections} />
        )}
        {tab === "search.label" && <MenuSearch />}
      </Box>
    </div>
  );
};
