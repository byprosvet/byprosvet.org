import { Collection } from "@/shop/product";

type Props = {
  collections: Collection[];
};

export const MenuCategories = ({ collections }: Props) => {
  return (
    <div>
      {collections.map(({ title }) => {
        return <div key={title}>{title}</div>;
      })}
    </div>
  );
};
