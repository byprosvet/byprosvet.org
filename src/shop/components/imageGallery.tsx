import { useEffect, useState } from "react";

import { tabletsAndUp } from "@/uikit/breakpoints";
import styled from "@emotion/styled";

import { FeaturedPreview } from "../product/featured-preview";
import { FeaturedImage } from "../product/model";

const Container = styled.div`
  grid-area: preview;
  flex-direction: column;
  align-self: start;
  padding: 0.5rem 0;
  ${tabletsAndUp`
    padding-right: 3vw;
  `}
  & > img {
    width: 100%;
    height: auto;
    aspect-ratio: 1 / 1;
    align-self: center;
  }
  grid-area: preview;
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  max-width: 100%;
`;

const Thumbnails = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  overflow-y: hidden;
  overflow-x: scroll;
  margin: 0.75rem 0 0 0;
  ${tabletsAndUp`
    margin: 1.5rem 0 0 0;
  `}
  height: auto;
  scrollbar-width: none;
  -ms-overflow-style: none;
  ::-webkit-scrollbar {
    width: 0;
    background: transparent;
  }
`;

const Thumbnail = styled.img`
  max-width: 6rem;
  max-height: 6rem;
  width: 100%;
  object-fit: cover;
  cursor: pointer;
  &:not(:first-of-type) {
    margin: 0 0 0 1.5rem;
  }
`;

interface ImageGalleryProps {
  images: FeaturedImage[];
}

export const ImageGallery = ({ images }: ImageGalleryProps) => {
  const [mainImage, setMainImage] = useState(images[0]);

  const handleThumbnailClick = (image: FeaturedImage) => {
    setMainImage(image);
  };

  useEffect(() => {
    setMainImage(images[0]);
  }, [images]);

  return (
    <Container>
      <FeaturedPreview featuredImage={mainImage} />
      <Thumbnails>
        {images.map(image => (
          <Thumbnail
            key={`thumbnail-${image.url}`}
            src={image.url}
            width={image.width}
            height={image.height}
            onClick={() => handleThumbnailClick(image)}
          />
        ))}
      </Thumbnails>
    </Container>
  );
};
