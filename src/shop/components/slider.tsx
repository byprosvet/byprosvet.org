"use client";

import { ReactNode, useEffect, useRef, useState } from "react";

import styled from "@emotion/styled";

interface SliderProps {
  children: ReactNode[];
}

const Slider = ({ children }: SliderProps) => {
  const [activeSlide, setActiveSlide] = useState(0);
  const sliderRef = useRef<HTMLDivElement>(null);

  const handleScroll = () => {
    if (sliderRef.current) {
      const index = Math.round(
        sliderRef.current.scrollLeft / sliderRef.current.clientWidth,
      );
      setActiveSlide(index);
    }
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      if (sliderRef.current) {
        const nextSlide = (activeSlide + 1) % children.length;
        sliderRef.current.scrollTo({
          left: sliderRef.current.clientWidth * nextSlide,
          behavior: "smooth",
        });
        setActiveSlide(nextSlide);
      }
    }, 5000);

    return () => clearTimeout(timer);
  }, [activeSlide, children.length]);

  return (
    <SliderWrapper>
      <SliderInner ref={sliderRef} onScroll={handleScroll}>
        {children.map((slide, index) => (
          <Slide key={`slide-${index}`}>{slide}</Slide>
        ))}
      </SliderInner>
      <Dots>
        {children.map((_, index) => (
          <Dot
            key={`dot-${index}`}
            active={index === activeSlide}
            onClick={() => {
              setActiveSlide(index);
              sliderRef.current?.scrollTo({
                left: sliderRef.current.clientWidth * index,
                behavior: "smooth",
              });
            }}
          />
        ))}
      </Dots>
    </SliderWrapper>
  );
};

const SliderWrapper = styled.div`
  width: 100%;
  position: relative;
  margin-bottom: 1rem;
`;

const SliderInner = styled.div`
  display: flex;
  overflow-x: scroll;
  scroll-snap-type: x mandatory;
  padding-bottom: 2rem;
  -webkit-overflow-scrolling: touch;

  &::-webkit-scrollbar {
    display: none;
  }
`;

const Slide = styled.div`
  width: 100%;
  flex-shrink: 0;
  scroll-snap-align: start;
`;

const Dots = styled.div`
  position: sticky;
  margin-top: -2rem;
  display: flex;
  justify-content: center;
  width: 100%;
  z-index: 1;
`;

const Dot = styled.div<{ active: boolean }>`
  width: 10px;
  height: 10px;
  background-color: #ccc;
  border-radius: 50%;
  margin: 0 5px;
  cursor: pointer;
  transition: background-color 0.3s ease;
  background-color: ${({ active }) => (active ? "#333" : "#ccc")};
`;

export default Slider;
