import { proxy, useSnapshot } from "valtio";

import { Product } from "../product";
import { getMinPrice } from "../product/model";

export type CartItem = {
  id: string;
  count: number;
  product: Product;
};

type CartState = {
  items: CartItem[];
};

const state = proxy<CartState>({ items: [] });

const countItems = () => state.items.reduce((acc, item) => acc + item.count, 0);

const createQuery = () =>
  state.items
    .map(item => `${item.id.split("/").pop()}:${item.count}`)
    .join(",");

const addToCart = (item: CartItem) => {
  const newCart = [...state.items];

  const foundedIndex = newCart.findIndex(
    newCartItem => newCartItem.id === item.id,
  );
  if (foundedIndex === -1) {
    newCart.push({ id: item.id, count: item.count, product: item.product });
  } else {
    const count = newCart[foundedIndex].count + item.count;
    newCart[foundedIndex] = { ...newCart[foundedIndex], count };
  }
  state.items = newCart;
};

const removeItem = (itemId: string) => {
  const newCart = state.items.filter(i => i.id !== itemId);
  state.items = newCart;
};

const changeCount = (itemId: string, newCount: number) => {
  if (newCount <= 0) {
    removeItem(itemId);
  } else {
    const newCart = state.items.map(i => {
      if (i.id === itemId) {
        return {
          ...i,
          count: newCount,
        };
      }
      return i;
    });
    state.items = newCart;
  }
};

const calculateTotalCost = () =>
  state.items.reduce(
    (total, item) => total + item.count * getMinPrice(item.product),
    0,
  );

export const useCart = () => {
  const { items } = useSnapshot(state);
  const count = countItems();
  const query = createQuery();
  const totalCost = calculateTotalCost();

  return {
    items,
    query,
    count,
    addToCart,
    removeItem,
    changeCount,
    totalCost,
  };
};

export type Cart = ReturnType<typeof useCart>;
