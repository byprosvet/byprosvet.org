import { Fragment } from "react";

import styled from "@emotion/styled";

const BreadcrumbsContainer = styled.div`
  display: flex;
  gap: 20px;
`;

const Step = styled.span<{ active?: boolean }>`
  color: ${props => (props.active ? "black" : "grey")};
`;

const Divider = styled.span`
  margin: 0 10px;
`;

interface BreadcrumbsProps {
  steps: string[];
  activeStep: string;
}

export const Breadcrumbs: React.FC<BreadcrumbsProps> = ({
  steps,
  activeStep,
}) => {
  return (
    <BreadcrumbsContainer>
      {steps.map((step, index) => (
        <Fragment key={step}>
          <Step active={step === activeStep}>{step}</Step>
          {index < steps.length - 1 && <Divider>{">"}</Divider>}
        </Fragment>
      ))}
    </BreadcrumbsContainer>
  );
};
