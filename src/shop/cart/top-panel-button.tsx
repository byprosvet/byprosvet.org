"use client";

import Link from "next/link";

import { useFeatureToggles } from "@/featureToggles";
import { colors } from "@/uikit";
import styled from "@emotion/styled";
import { ShoppingCart } from "@mui/icons-material";

import { Cart, useCart } from "./store";

const goToShopify = (cart: Cart) => {
  if (cart.count < 1) {
    return;
  }
  window.location.href = `https://cart.byprosvet.org/cart/${cart.query}`;
};

export const CartButton = () => {
  const cart = useCart();
  const { count } = cart;
  const { isCartEnabled } = useFeatureToggles();
  return (
    <CartBlock>
      {isCartEnabled ? (
        <Link href="/cart">
          <ShoppingCart htmlColor={colors.fontSecondary} />
        </Link>
      ) : (
        <ShoppingCart
          onClick={() => goToShopify(cart)}
          htmlColor={colors.fontSecondary}
        />
      )}
      {count > 0 && <Badge>{count}</Badge>}
    </CartBlock>
  );
};

export default CartButton;

const CartBlock = styled.div`
  position: relative;
  cursor: pointer;
  height: 31px;
  padding-top: 8px;
`;

const Badge = styled.div`
  position: absolute;
  width: 16px;
  height: 16px;
  background-color: ${colors.backgroundMain};
  top: -0.5rem;
  right: -0.5rem;
  font-size: 0.75rem;
  font-weight: 600;
  border-radius: 50%;
  border: 1px solid ${colors.backgroundMain};
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${colors.fontHighlight};
`;
