"use client";

import { PropsWithChildren, useState } from "react";

import { t } from "@/i18n";
import { Button, ReverseButton } from "@/uikit/button";
import { CheckIcon, ShoppingCart } from "@/uikit/icons";

import { Product } from "../product";
import { useCart } from "./store";

type Props = {
  id: string;
  product: Product;
  quantity: number;
  onAdd: () => void;
};

export const AddToCartButton = (props: PropsWithChildren<Props>) => {
  const [added, setAdded] = useState(false);
  const { addToCart } = useCart();

  const add = () => {
    addToCart({ id: props.id, count: props.quantity, product: props.product });
    props.onAdd();
    setAdded(true);
    setTimeout(() => {
      setAdded(false);
    }, 1500);
  };
  if (added) {
    return (
      <ReverseButton>
        <CheckIcon />
        <span>{t("cart.addedToCart")}</span>
      </ReverseButton>
    );
  }
  return (
    <Button onClick={add}>
      <ShoppingCart />
      <span>{props.children}</span>
    </Button>
  );
};
