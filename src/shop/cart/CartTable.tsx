"use client";

import { useCallback } from "react";

import Image from "next/image";
import { useRouter } from "next/navigation";

import { t } from "@/i18n";
import { startCheckout } from "@/shop/checkout/session";
import { ActionBottomButtons } from "@/uikit/actionBottomButtons";
import { Breadcrumbs } from "@/uikit/breadcrumbs";
import { DeleteIcon } from "@/uikit/icons";
import styled from "@emotion/styled";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Typography from "@mui/material/Typography";

import { getMinPrice } from "../product/model";
import { OrderQuantityControl } from "../product/quantity";
import { useCart } from "./store";

const breadcrumbItems = [
  { label: t("cart"), active: true },
  { label: t("checkout.info") },
  { label: t("checkout.delivery") },
  { label: t("checkout.payment") },
];

export const Cart = () => {
  const cart = useCart();
  const router = useRouter();
  const toCheckout = useCallback(() => {
    startCheckout("", "", cart.items);
    router.push("/checkout");
  }, [cart.items, startCheckout, router]);

  return (
    <FixedHeightContainer sx={{ px: 4 }}>
      <Breadcrumbs items={breadcrumbItems} />
      <Title>{t("cart")}</Title>
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <StyledTableCell>
                <TableHeaderTypography>{t("product")}</TableHeaderTypography>
              </StyledTableCell>
              <StyledTableCell align="right">
                <TableHeaderTypography>{t("cart.count")}</TableHeaderTypography>
              </StyledTableCell>
              <StyledTableCell align="right">
                <TableHeaderTypography>{t("cart.total")}</TableHeaderTypography>
              </StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {cart.items.map(item => (
              <TableRow key={item.product.id}>
                <StyledTableCell>
                  <Box display="flex" alignItems="flex-start">
                    <Image
                      src={item.product.featuredImage!.url}
                      width={120}
                      height={120}
                      alt={item.product.title}
                    />
                    <Box ml={1.5}>
                      <Typography
                        color="textPrimary"
                        variant="subtitle1"
                        style={{
                          fontSize: "1rem",
                          fontFamily: "Montserrat",
                          fontWeight: 500,
                          lineHeight: "1.25rem",
                        }}
                      >
                        {item.product.title}
                      </Typography>
                      <Box mt={0.5}>
                        <Typography
                          color="textSecondary"
                          variant="subtitle2"
                          style={{
                            fontSize: "0.875rem",
                            fontFamily: "Montserrat",
                            fontWeight: 500,
                            lineHeight: "1.125rem",
                          }}
                        >
                          {getMinPrice(item.product)}zl
                        </Typography>
                      </Box>
                    </Box>
                  </Box>
                </StyledTableCell>
                <StyledTableCell align="right">
                  <Box display="flex" alignItems="center">
                    <OrderQuantityControl
                      count={item.count}
                      onChange={count => cart.changeCount(item.id, count)}
                    />
                    <StyledIconButton
                      onClick={() => cart.removeItem(item.id)}
                      color="primary"
                    >
                      <DeleteIcon />
                    </StyledIconButton>
                  </Box>
                </StyledTableCell>
                <StyledTableCell align="right">
                  <TotalPriceTypography>
                    {item.count * getMinPrice(item.product)}zl
                  </TotalPriceTypography>
                </StyledTableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <SummaryBoxContainer>
        <SummaryBox>
          <Box className="summary-row">
            <Typography className="summary-title">
              {t("cart.subtotal")}
            </Typography>
            <Typography className="summary-cost">{cart.totalCost}zl</Typography>
          </Box>
          <Box className="summary-row">
            <Typography className="delivery-title">
              {t("checkout.delivery")}
            </Typography>
            <Typography className="delivery-cost">
              {t("cart.calculatedLater")}
            </Typography>
          </Box>
        </SummaryBox>
      </SummaryBoxContainer>
      <Box flexGrow={1}></Box>
      <ActionBottomButtons
        backLabel={t("cart.backToShopping")}
        onBackClick={() => router.push("/")}
        forwardLabel={t("cart.forwardToCheckout")}
        onForwardClick={toCheckout}
      />
    </FixedHeightContainer>
  );
};

const StyledTableCell = styled(TableCell)`
  padding: 1rem 0;
`;

const TableHeaderTypography = styled(Typography)`
  color: var(--grey, #676767);
  font-size: 0.875rem;
  font-family: Montserrat;
  font-style: normal;
  font-weight: 400;
  line-height: 1.125rem;
`;

const TotalPriceTypography = styled(Typography)`
  color: var(--black, #000);
  font-size: 0.875rem;
  font-family: Montserrat;
  font-style: normal;
  font-weight: 500;
  line-height: 1.125rem;
`;

export const Title = styled.h1`
  font-size: 2rem;
  font-family: Montserrat;
  font-style: normal;
  font-weight: 700;
  line-height: 2.5rem;
`;
const StyledIconButton = styled(IconButton)`
  color: #00aeef;
  margin-left: 1rem;
`;

const SummaryBoxContainer = styled(Box)`
  width: 100%;
  display: flex;
  justify-content: flex-end;
`;

const SummaryBox = styled(Box)`
  width: 25rem;
  margin-top: 1rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  gap: 0.75rem;

  .summary-row {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .summary-title {
    color: var(--black, #000);
    font-size: 1.125rem;
    font-family: Montserrat;
    font-style: normal;
    font-weight: 500;
    line-height: 1.375rem;
  }

  .delivery-title {
    color: var(--black, #000);
    font-size: 0.875rem;
    font-family: Montserrat;
    font-style: normal;
    font-weight: 500;
    line-height: 1.125rem;
  }

  .summary-cost {
    color: var(--black, #000);
    font-size: 0.875rem;
    font-family: Montserrat;
    font-style: normal;
    font-weight: 500;
    line-height: 1.125rem;
  }

  .delivery-cost {
    color: var(--light-grey, #979797);
    font-size: 0.875rem;
    font-family: Montserrat;
    font-style: normal;
    font-weight: 500;
    line-height: 1.125rem;
  }
`;

export const FixedHeightContainer = styled(Box)`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-height: calc(100vh - 14.93rem);
  overflow-y: scroll;
  &:first-of-type {
    flex-grow: 1;
  }
`;
