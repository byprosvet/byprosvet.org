"use client";

import { useState } from "react";

import styled from "@emotion/styled";

import { colors } from "../uikit";
import { CreditCardIcon } from "../uikit/icons";

export const Donations = () => {
  return (
    <>
      <h1 id="Поддержать">Падтрымаць далярам</h1>
      <div>
        <p>
          ByProsvet - беларускі фонд зарэгістраваны ў Еўрапейскім Саюзе.
          Займаецца фінансаваннем, арганізацыяй сеткі друку, распаўсюджвання
          газет, улётак і налепак у Беларусі.{" "}
        </p>
        <p>Вы можаце падтрымаць нашу працу праз:</p>
        <ul>
          <li>
            BTC:{" "}
            <WalletAddress>1NAnmo5sJVzeqpzaMfVUCJ4zo53XADS1sE</WalletAddress>
          </li>
          <li>
            ETH:{" "}
            <WalletAddress>
              0xe3CeAec108AbF23985880218daF0BE262Cf41300
            </WalletAddress>
          </li>
          <li>
            USDC ETH:{" "}
            <WalletAddress>
              0xD2B277c2FE3B4334A6DA9eaB345ddaDf6208D2E0
            </WalletAddress>
          </li>
          <li>
            USDT ETH:{" "}
            <WalletAddress>
              0xD2B277c2FE3B4334A6DA9eaB345ddaDf6208D2E0
            </WalletAddress>
          </li>
          <li>
            USDT TRX:{" "}
            <WalletAddress>TAv9wzrEAtzhRbcDdQAYbUcWc5k1M5tUhi</WalletAddress>
          </li>
          <li>
            USDC TRX:{" "}
            <WalletAddress>TAv9wzrEAtzhRbcDdQAYbUcWc5k1M5tUhi</WalletAddress>
          </li>
          <li>
            IBAN PLN:{" "}
            <WalletAddress>PL86 1020 1042 0000 8402 0509 1436</WalletAddress>
          </li>
          <li>
            IBAN EUR:{" "}
            <WalletAddress>PL91 1020 1042 0000 8202 0509 1444</WalletAddress>
          </li>
          <li>
            <a href="https://bysol.org/ru/initiatives/byprosvet/">
              наш сбор на BySol
            </a>
          </li>
          <li>
            <a href="https://buy.stripe.com/bIYg1u4WieNG4vu000">
              падпіску на рэгулярныя штомесячныя ахвяраванні
            </a>
          </li>
        </ul>
      </div>
    </>
  );
};

const WalletAddress = styled.span`
  font-family: monospace;
  background-color: #ddd;
  font-size: 0.8rem;
  @media (min-width: 480px) {
    font-size: 0.95rem;
  }
`;

export const Donations_new = () => {
  return (
    <form>
      <Container>
        <h1 id="support">Поддержать проект</h1>
        <div>
          Вы можете поддержать выпуск самиздата отправив на пожертвования:
        </div>
        <DonationsBlock
          for="Политзаключенным"
          description="На оплату услуг адвокатов"
        />
        <DonationsBlock
          for="В самиздат"
          description="На печать газет и листовок"
        />
        <DonationsBlock
          for="На выпуск книг"
          description="На выпуск новых книг"
        />
        <div>
          <label>
            <input type="checkbox" name="subscribe_me" />Я хочу помогать
            ежемесячно
          </label>
        </div>
      </Container>
      <SubmitContainer>
        <SubmitButton />
      </SubmitContainer>
    </form>
  );
};

const Container = styled.div`
  background-color: ${colors.backgroundHighlight};
  text-align: left;
  padding: 1rem 5vw;
  h1 {
    text-transform: uppercase;
    font-size: 2rem;
    letter-spacing: 1px;
  }
`;

const DonationsBlock = (props: { for: string; description: string }) => {
  const [value, setValue] = useState("5");
  const min = 0;
  const max = 100;
  return (
    <DonationType>
      <h2>{props.for}</h2>
      <div>
        <Range
          type="range"
          name={`range-${props.for}`}
          value={value}
          min={min}
          max={max}
          style={{ backgroundSize: `${value}% 100%` }}
          onChange={e => setValue(e.target.value)}
        />
        <select name={`amount-${props.for}`}>
          <option value={value}>{value} zl</option>
          <option value={value + 1}>{value + 1} zl</option>
        </select>
      </div>
      <p>{props.description}</p>
    </DonationType>
  );
};

const DonationType = styled.div`
  h2 {
    font-weight: 600;
    font-size: 24px;
    line-height: 29px;
  }
  select {
    width: 23vw;
    height: 48px;
    color: #000;
    background: #fff;
    border: 1px solid #cecece;
    border-radius: 4px;
    font-weight: 600;
    font-size: 20px;
    padding: 13px 20px 11px 13px;
  }
`;

const Range = styled.input`
  -webkit-appearance: none;
  margin-right: 15px;
  width: 60vw;
  height: 2px;
  background: #cecece;
  background-image: linear-gradient(#00aeef, #00aeef);
  background-size: 0% 100%;
  background-repeat: no-repeat;

  &::-webkit-slider-thumb {
    -webkit-appearance: none;
    height: 24px;
    width: 24px;
    border-radius: 50%;
    background: #fff;
    cursor: ew-resize;
    box-shadow: 0 0 2px 0 #555;
    transition: background 0.3s ease-in-out;
  }

  &::-webkit-slider-runnable-track {
    -webkit-appearance: none;
    box-shadow: none;
    border: none;
    background: transparent;
  }
`;

const SubmitContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin: 1rem auto;
`;
const SubmitButton = () => (
  <Button type="submit">
    <CreditCardIcon />
    <span>Перейти к оплате</span>
  </Button>
);
const Button = styled.button`
  display: flex;
  flex-direction: row;
  color: ${colors.fontHighlight};
  background-color: ${colors.backgroundSecondary};
  border-radius: 4px;
  border: 0;
  padding: 0;
  margin: 0.75rem 0;
  font-size: 1rem;
  cursor: pointer;
  svg {
    display: inline-block;
    margin: 0.75rem 0.5rem 0.75rem 1rem;
  }
  span {
    display: inline-block;
    margin: 14px 1rem 14px 0;
  }
`;
