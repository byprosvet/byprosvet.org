import { containerPadding } from "@/uikit/layout";
import styled from "@emotion/styled";

export const Footer = () => (
  <Container>
    <LeftBlock>
      <p>&#169; 2022-2024 Fundacja ByProsvet</p>
      <p>Belarusian culture and media support</p>
    </LeftBlock>
    <RightBlock>
      <p>Numer KRS: 0000977925</p>
      <p>NIP: 8971907793, REGON: 522377692</p>
    </RightBlock>
  </Container>
);

const Container = styled.footer`
  position: relative;
  z-index: 100;
  display: flex;
  background-color: #000;
  color: #fff;
  flex-direction: column;
  ${containerPadding}
  padding-bottom: 1.5rem;
  padding-top: 1.5rem;
  @media (min-width: 640px) {
    flex-direction: row;
  }
`;
const LeftBlock = styled.div`
  flex-grow: 7;
`;
const RightBlock = styled.div`
  flex-grow: 1;
`;
