"use client";

import { useMemo } from "react";

import Link from "next/link";

import { t } from "@/i18n";
import { Breadcrumbs, breadcrumbsWithHome } from "@/uikit/breadcrumbs";
import { ReverseButton } from "@/uikit/button";
import styled from "@emotion/styled";
import LaunchIcon from "@mui/icons-material/Launch";

import { AddToCartButton } from "../cart";
import { ImageGallery } from "../components/imageGallery";
import { BuyArea, DescriptionArea, PreviewWithDescription } from "../layout";
import { getCompareAtPrice, getMinPrice, getVariantId } from "./model";
import type { Product } from "./model";
import { OrderQuantityControl, usePrice } from "./quantity";

type Props = {
  product: Product;
};

export const FullProduct = ({ product }: Props) => {
  const originalPrice = getMinPrice(product);
  const variantId = useMemo(() => getVariantId(product), [product]);
  const compareAtPrice = getCompareAtPrice(product);

  const { price, quantity, setQuantity } = usePrice(originalPrice);
  const images = product.images;
  return (
    <PreviewWithDescription>
      <Breadcrumbs
        items={breadcrumbsWithHome.concat([{ label: product.title }])}
      />
      <ImageGallery images={images} />
      <DescriptionArea>
        <h1>{product.title}</h1>
        <div
          dangerouslySetInnerHTML={{
            __html: product.descriptionHtml,
          }}
        />
      </DescriptionArea>
      <BuyArea>
        <OrderQuantityControl onChange={setQuantity} count={quantity} />
        <h2>
          {compareAtPrice ? <s>{compareAtPrice}zl</s> : ""}
          {originalPrice}zl {quantity > 1 ? `* ${quantity} = ${price}zł` : ""}
        </h2>
        {product.redirect ? (
          <Button href={product.redirect} target="_blank" small>
            <span>{t("product.redirect")}</span> <LaunchIcon />
          </Button>
        ) : (
          <AddToCartButton
            id={variantId}
            quantity={quantity}
            product={product}
            onAdd={() => setQuantity(quantity)}
          >
            {t("cart.addToCart")}
          </AddToCartButton>
        )}
      </BuyArea>
    </PreviewWithDescription>
  );
};

const Button = styled(ReverseButton.withComponent(Link))`
  width: 20vw;
  @media (max-width: 1000px) {
    width: 30vw;
  }
  @media (max-width: 800px) {
    width: 40vw;
  }
  @media (max-width: 500px) {
    width: 50vw;
  }

  justify-content: center;

  svg {
    width: 1.5rem;
    height: 1.5rem;
    margin-top: -0.05rem;
    margin-left: 0.5rem;
  }
`;
