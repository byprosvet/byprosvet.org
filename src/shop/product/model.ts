export type Variant = {
  id: string;
};

export type VariantPrice = {
  amount: string;
  currencyCode: string;
};

export type PriceRange = {
  maxVariantPrice?: VariantPrice;
  minVariantPrice: VariantPrice;
};

export type FeaturedImage = {
  height: number;
  width: number;
  url: string;
};

export type Collection = {
  title: string;
  handle: string;
  description: string;
  image: FeaturedImage;
  products: Product[];
};

export type Product = {
  gid: string;
  id: string;
  featuredImage?: FeaturedImage;
  images: FeaturedImage[];
  priceRange: PriceRange;
  compareAtPriceRange: PriceRange;
  title: string;
  descriptionHtml: string;
  variants?: Variant[];
  redirect?: string;
};

type GetMinPrice = (p: Pick<Product, "priceRange">) => number;
export const getMinPrice: GetMinPrice = product =>
  parseFloat(product.priceRange.minVariantPrice.amount);

export const getCompareAtPrice = (product: Product) => {
  const price = product.compareAtPriceRange?.maxVariantPrice?.amount;
  if (!price) return null;
  return parseFloat(price);
};

export const getVariantId = (product: Product) => {
  return product.variants?.[0]?.id ?? "";
};
