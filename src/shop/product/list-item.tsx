"use client";

import Link from "next/link";

import { t } from "@/i18n";
import { colors } from "@/uikit";
import { ReverseButton } from "@/uikit/button";
import styled from "@emotion/styled";
import LaunchIcon from "@mui/icons-material/Launch";

import { AddToCartButton } from "../cart";
import { FeaturedPreview } from "./featured-preview";
import { getCompareAtPrice, getMinPrice, getVariantId } from "./model";
import type { Product } from "./model";
import { OrderQuantityControl, usePrice } from "./quantity";

type ProductProps = {
  product: Product;
};

export const SmallProductItem = ({ product }: ProductProps) => {
  const { price, quantity, setQuantity } = usePrice(getMinPrice(product));
  const compareAtPrice = getCompareAtPrice(product);

  const variantId = getVariantId(product);

  return (
    <Container>
      <ImageContainer href={`/product/${product.id}`}>
        <FeaturedPreview
          featuredImage={product.featuredImage}
          alt={`Image for ${product.title}`}
        />
        <Title>{product.title}</Title>
      </ImageContainer>
      <InformationContainer>
        <OrderQuantityControl
          count={quantity}
          onChange={setQuantity}
        ></OrderQuantityControl>

        <Price>
          {compareAtPrice ? <s>{compareAtPrice}zl</s> : ""}
          {price}zl{" "}
        </Price>

        <ButtonContainer>
          {product.redirect ? (
            <Button href={product.redirect || ""} target="_blank" small>
              <span>{t("product.redirect")}</span> <LaunchIcon />
            </Button>
          ) : (
            <AddToCartButton
              id={variantId}
              quantity={quantity}
              product={product}
              onAdd={() => setQuantity(1)}
            >
              У кошык
            </AddToCartButton>
          )}
        </ButtonContainer>
      </InformationContainer>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-width: 15rem;
  width: 15rem;

  @media (max-width: 711px) {
    min-width: calc(50% - 1rem);
    width: calc(50% - 1rem);
  }
  @media (max-width: 462px) {
    min-width: 80%;
    width: 80%;
  }
`;

const InformationContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const ButtonContainer = styled.div`
  width: 100%;
  max-width: 100%;
`;

export const ImageContainer = styled(Link)`
  display: block;
  max-width: 100%;
  color: ${colors.fontMain};
  img {
    max-width: 100%;
    height: auto;
    aspect-ratio: 1;
  }
`;

const Title = styled.p`
  font-family: "Montserrat";
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 22px;
`;

const Price = styled.h3`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  margin: 8px 0;
  s {
    color: rgb(188, 187, 185);
    margin-right: 16px;
  }
`;

const Button = styled(ReverseButton.withComponent(Link))`
  justify-content: center;

  svg {
    width: 1.5rem;
    height: 1.5rem;
    margin-top: -0.05rem;
    margin-left: 0.5rem;
  }
`;
