"use client";

import { useMemo } from "react";

import Image from "next/image";
import Link from "next/link";

import { t } from "@/i18n";
import { ReverseButton } from "@/uikit/button";
import styled from "@emotion/styled";
import LaunchIcon from "@mui/icons-material/Launch";

import { AddToCartButton } from "../cart";
import {
  BuyArea,
  DescriptionArea,
  ImageArea,
  PreviewWithDescription,
} from "../layout";
import { getCompareAtPrice, getMinPrice, getVariantId } from "./model";
import type { Product } from "./model";
import { OrderQuantityControl, usePrice } from "./quantity";

type Props = {
  product: Product;
};

export const Promotion = ({ product }: Props) => {
  const originalPrice = getMinPrice(product);
  const compareAtPrice = getCompareAtPrice(product);
  const variantId = useMemo(() => getVariantId(product), [product]);

  const { price, quantity, setQuantity } = usePrice(originalPrice);
  const { featuredImage } = product;

  return (
    <PreviewWithDescription>
      <ImageArea>
        {featuredImage && (
          <Link href={`/product/${product.id}`}>
            <Image
              src={featuredImage.url}
              alt={`Image for ${product.title}`}
              width={featuredImage.width}
              height={featuredImage.height}
            />
          </Link>
        )}
      </ImageArea>
      <DescriptionArea>
        <h1>{product.title}</h1>
        <Description
          dangerouslySetInnerHTML={{
            __html: product.descriptionHtml,
          }}
        />
      </DescriptionArea>
      <BuyArea>
        <OrderQuantityControl onChange={setQuantity} count={quantity} />
        <h2>
          {compareAtPrice ? <s>{compareAtPrice}zl</s> : ""}
          {originalPrice}zl {quantity > 1 ? `* ${quantity} = ${price}zł` : ""}
        </h2>
        {product.redirect ? (
          <Button href={product.redirect || ""} target="_blank" small>
            <span>{t("product.redirect")}</span> <LaunchIcon />
          </Button>
        ) : (
          <AddToCartButton
            id={variantId}
            quantity={quantity}
            product={product}
            onAdd={() => setQuantity(quantity)}
          >
            {t("cart.addToCart")}
          </AddToCartButton>
        )}
      </BuyArea>
    </PreviewWithDescription>
  );
};

const Description = styled.div`
  display: -webkit-box;
  -webkit-line-clamp: 7;
  -webkit-box-orient: vertical;
  overflow: hidden;
  position: relative;
  &::after {
    content: "";
    background-image: linear-gradient(
      to bottom,
      rgba(248, 247, 245, 0) 50%,
      rgba(248, 247, 245, 1) 100%
    );
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    height: 50%;
    pointer-events: none;
  }
  @media (max-width: 440px) {
    display: none;
  }
`;

const Button = styled(ReverseButton.withComponent(Link))`
  width: 20vw;
  @media (max-width: 400px) {
    width: 50vw;
  }
  @media (max-width: 800px) {
    width: 40vw;
  }
  @media (max-width: 1000px) {
    width: 30vw;
  }

  justify-content: center;

  svg {
    width: 1.5rem;
    height: 1.5rem;
    margin-top: -0.05rem;
    margin-left: 0.5rem;
  }
`;
