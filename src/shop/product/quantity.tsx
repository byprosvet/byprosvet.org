import { useState } from "react";

import { AddIcon, RemoveIcon } from "@/uikit/icons";
import styled from "@emotion/styled";

export interface OrderCountControlProps {
  count: number;
  onChange: (value: number) => void;
}

export const OrderQuantityControl = ({
  count,
  onChange,
}: OrderCountControlProps) => {
  const increment = () => onChange(count + 1);
  const decrement = () => {
    if (count === 1) {
      return;
    }

    onChange(count - 1);
  };

  return (
    <OrderQuantityWrap>
      <PlusMinusButton onClick={decrement}>
        <RemoveIcon />
      </PlusMinusButton>
      <OrderQuantity>{count}</OrderQuantity>
      <PlusMinusButton onClick={increment}>
        <AddIcon />
      </PlusMinusButton>
    </OrderQuantityWrap>
  );
};

export const usePrice = (originalPrice: number) => {
  const [quantity, setQuantity] = useState(1);

  const price = originalPrice * quantity;

  return { price, setQuantity, quantity };
};

const OrderQuantityWrap = styled.div`
  height: 48px;
  width: 96px;
  border-radius: 4px;
  padding: 0 10px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border: 1px solid #cecece;
  box-sizing: border-box;
  background-color: #fff;
`;

const PlusMinusButton = styled.button`
  border: none;
  background: none;
  padding: 0;

  > svg {
    color: #6d7076;
    font-size: 18px;
    vertical-align: middle;
  }
`;

const OrderQuantity = styled.div`
  font-weight: 600;
  font-size: 20px;
  line-height: 24px;
  color: #0b111a;
  text-align: center;
  min-width: 0;
  text-overflow: ellipsis;
  overflow: hidden;
`;
