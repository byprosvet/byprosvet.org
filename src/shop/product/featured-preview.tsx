import Image from "next/image";

import { FeaturedImage } from "./model";

type Props = {
  featuredImage?: FeaturedImage;
  alt?: string;
  width?: number;
  height?: number;
};

export const FeaturedPreview = ({
  featuredImage,
  alt,
  width,
  height,
}: Props) => {
  if (!featuredImage) return null;
  return (
    <Image
      key={featuredImage.url}
      src={featuredImage.url}
      width={width ?? featuredImage.width}
      height={height ?? featuredImage.height}
      alt={alt || ""}
    />
  );
};
