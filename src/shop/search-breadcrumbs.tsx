"use client";

import { t } from "@/i18n";
import { Breadcrumbs, breadcrumbsWithHome } from "@/uikit/breadcrumbs";
import { containerPadding } from "@/uikit/layout";
import styled from "@emotion/styled";

type Props = {
  itemsCount: number;
};

export const SearchPageBreadcrumbs = ({ itemsCount }: Props) => {
  const breadcrumbs = [
    ...breadcrumbsWithHome,
    { label: `${t("search.result.products")}: ${itemsCount}` },
  ];
  return (
    <Container>
      <Breadcrumbs items={breadcrumbs} />
    </Container>
  );
};
const Container = styled.div`
  display: block;
  width: 100%;
  ${containerPadding}
  margin-top: 0;
`;
