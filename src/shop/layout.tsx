import { smallDesktopAndUp, tabletsAndUp } from "@/uikit/breakpoints";
import { containerPadding } from "@/uikit/layout";
import styled from "@emotion/styled";

export const PreviewWithDescription = styled.div`
  ${containerPadding}
  padding-bottom: 2rem;
  display: grid;
  background-color: #f8f7f5;
  grid-template-areas:
    "breadcrumbs breadcrumbs"
    "preview preview"
    "description description"
    "buy buy";
  grid-template-columns: 35% 65%;
  height: 100%;
  ${tabletsAndUp`
    grid-template-areas:
      "breadcrumbs breadcrumbs"
      "preview description"
      "preview buy"
      "preview .";
  `}

  p {
    margin: 0.5rem 0;
    font-size: 14px;
  }
`;

export const ImageArea = styled.div`
  grid-area: preview;
  justify-content: center;
  align-items: self-start;
  display: flex;
  img {
    width: auto;
    height: 320px;
  }
  padding: 0;
  ${tabletsAndUp`
    padding: 2.5rem 5vw 0 0;
    img {
      width: 100%;
      height: auto;
    }
  `}
`;

export const DescriptionArea = styled.div`
  grid-area: description;
  padding: 0;
  line-height: 1.5rem;
  ${tabletsAndUp`
    padding: 1rem 0vw 0 0vw;
  `}

  h1 {
    margin: 0.5rem 0;
    font-size: 1.25rem;
    line-height: 1.5rem;
    ${smallDesktopAndUp`
      font-size: 2rem;
      margin: 1rem 0;
      line-height: 2rem;
    `}
    font-weight: 700;
  }

  h2 {
    display: flex;
    flex-direction: column;
    ${tabletsAndUp`
      display: block;
    `}
    line-height: 24px;
    font-size: 16px;
    font-weight: 600;
    a {
      display: inline-block;
      color: #000;
      text-decoration: none;
      margin-right: 24px;
      svg {
        margin-bottom: -6px;
        margin-left: 8px;
      }
    }
  }
  & > div {
    color: #373737;
    font-size: 16px;
  }
`;

export const BuyArea = styled.div`
  grid-area: buy;
  align-self: start;
  display: block;
  h2 {
    font-size: 20px;
    s {
      color: rgb(188, 187, 185);
      margin-right: 16px;
    }
  }
`;
