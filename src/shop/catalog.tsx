"use client";

import Link from "next/link";

import { t } from "@/i18n";
import { Carousel } from "@/shop/components/Common/Carousel/Carousel";
import { ReverseButton } from "@/uikit/button";
import { ArrowForward } from "@/uikit/icons";
import { containerPadding } from "@/uikit/layout";
import { Title } from "@/uikit/title";
import { css } from "@emotion/react";
import styled from "@emotion/styled";

import { Collection, Product, SmallProductItem } from "./product";

type BooksProps = {
  products: Product[];
};

type Props = {
  collections: Array<Collection>;
};

export const Collections = ({ collections }: Props) => {
  return (
    <Container>
      {collections.map(collection => (
        <CollectionContainer key={collection.title}>
          <TitleContainer>
            <Title textAlign="left">{collection.title}</Title>
            <Button href={`/collection/${collection.handle}`} small>
              <span>{t("collections.more")}</span> <ArrowForward />
            </Button>
          </TitleContainer>
          <Carousel
            containerStyles={css`
              > * {
                padding-right: 2rem;
              }
            `}
            data={collection.products || []}
            renderItem={product => {
              return <SmallProductItem key={product.id} product={product} />;
            }}
          />
        </CollectionContainer>
      ))}
    </Container>
  );
};

export const Catalog = ({ products }: BooksProps) => {
  return (
    <Container>
      <ProductsContainer>
        {products.map(product => (
          <SmallProductItem key={product.id} product={product} />
        ))}
      </ProductsContainer>
    </Container>
  );
};

const TitleContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  ${containerPadding}
`;

const Container = styled.div`
  padding-top: 2rem;
`;

export const ProductsContainer = styled.div`
  display: flex;
  flex-direction: row;
  gap: 2rem;
  justify-content: space-between;
  flex-wrap: wrap;
  ${containerPadding}
  padding-bottom: 4rem;
  padding-top: 2.5rem;
  background-color: #fff;
  &::after {
    content: "";
    flex: auto;
  }
`;

const CollectionContainer = styled.div``;

const Button = styled(ReverseButton.withComponent(Link))`
  svg {
    width: 1.5rem;
    height: 1.5rem;
    margin-top: -0.05rem;
    margin-left: 0.5rem;
  }
`;
