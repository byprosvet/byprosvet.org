"use client";

import { ChangeEvent, useCallback, useEffect, useRef, useState } from "react";

import { usePathname, useRouter, useSearchParams } from "next/navigation";

import { useOnClickOutside } from "@/hooks/useOnClickOutside";
import { t } from "@/i18n";
import { colors } from "@/uikit";
import {
  landscapeMobilesAndUp,
  mobileAndDown,
  smallDesktopAndUp,
} from "@/uikit/breakpoints";
import { cx } from "@emotion/css";
import styled from "@emotion/styled";
import SearchIcon from "@mui/icons-material/Search";
import FormHelperText from "@mui/material/FormHelperText";
import OutlinedInput from "@mui/material/OutlinedInput";

export const GlobalSearch = () => {
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const q = searchParams.get("q");
  const [search, setSearch] = useState(q ?? "");
  const [error, setError] = useState("");
  const [isOpened, setIsOpened] = useState(false);
  const searchInputContainer = useRef<HTMLDivElement>();
  const formRefContainer = useRef<HTMLFormElement>();

  const onClose = useCallback(() => {
    setIsOpened(false);
    setError("");
  }, []);

  const onSubmit = (e: any) => {
    e.preventDefault();

    if (!search) return;
    if (search.length < 2) {
      return setError(t("search.error.shortInput"));
    }

    onClose();

    if (pathname === "/search") {
      return router.replace(`/search?q=${search}`);
    }
    router.push(`/search?q=${search}`);
  };

  const onFocus = () => {
    setIsOpened(true);
  };

  const onChange = ({ target: { value } }: ChangeEvent<HTMLInputElement>) => {
    setError("");
    setSearch(value);
  };

  const focusInput = (e: any) => {
    e.preventDefault();

    const input = searchInputContainer?.current?.querySelector("input");
    if (input) {
      input.focus();
      input.selectionStart = input.selectionEnd = search.length;
    }
  };

  useEffect(() => {
    setSearch((q as string) || "");
  }, [q]);

  useOnClickOutside([formRefContainer], isOpened, onClose);

  return (
    <SearchContainer
      onSubmit={onSubmit}
      className={cx({ ["search__opened"]: isOpened })}
      ref={formRefContainer as any}
    >
      <SearchInput
        size="small"
        aria-describedby="search"
        placeholder={t("search.label")}
        inputProps={{
          "aria-label": "search",
        }}
        value={search}
        onChange={onChange}
        error={!!error}
        type="search"
        ref={searchInputContainer}
        onFocus={onFocus}
      />
      <SearchButton type="submit" onClick={onSubmit}>
        <SearchIcon />
      </SearchButton>
      <MobileSearchFocusButton onClick={focusInput}>
        <SearchIcon />
      </MobileSearchFocusButton>
      {!!error && <ErrorHint>{error}</ErrorHint>}
    </SearchContainer>
  );
};

const SearchContainer = styled.form`
  display: flex;
  align-items: center;
  flex-direction: row;
  position: relative;
  max-width: 100%;
  transition: width 0.2s;
  height: 50px;
  button[type="submit"] {
    display: none;
  }
  button:not([type="submit"]) {
    z-index: 2;
  }
  ,
  &.search__opened {
    width: 95vw;
    padding: 0 2.5vw 0 0;
    margin-left: 2.5vw;
    position: absolute;
    left: 0;
    z-index: 1001;
    background-color: white;
    button:not([type="submit"]) {
      display: none;
    }
    button[type="submit"] {
      display: block;
    }
    div:has(input) {
      width: 100%;
      opacity: 1;
    }
  }
  div:has(input) {
    width: 0;
    opacity: 0;
  }
  ${smallDesktopAndUp`
      height: 80px;
  `}
  ${landscapeMobilesAndUp`
    div:has(input) {
      width: 100%;
      opacity: 1;
    }
    button:not([type="submit"]) {
      display: none;
    }
    button[type="submit"] {
      display: block;
    }
    width: 45vw;
  `}
  ${smallDesktopAndUp`
    width: 400px;
  `}
`;

const ErrorHint = styled(FormHelperText)`
  position: absolute;
  bottom: -1px;
  left: 0;
  color: red;
  ${mobileAndDown`
    bottom: -15px;
  `}
`;

const MobileSearchFocusButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 39px;
  width: 45px;
  background-color: #fff;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  cursor: pointer;
  border: none;

  svg {
    color: ${colors.backgroundSecondary};
  }
`;

const SearchButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${colors.backgroundSecondary};
  height: 39px;
  width: 45px;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  cursor: pointer;
  border: none;

  svg {
    color: #fff;
  }
`;

const SearchInput = styled(OutlinedInput)`
  padding-right: 0;
  height: 39px;
  display: flex;
  flex-grow: 1;
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
  border-right: none;
  fieldset {
    border-right: none;
  }
  &:has(input:focus) {
    &:not(.Mui-error) {
      fieldset {
        border-color: ${colors.backgroundSecondary} !important;
      }
    }
    ${mobileAndDown`
      &:has(input:focus) {
        fieldset {
        top: -4px;
      }
    }
  `}
  }
`;
