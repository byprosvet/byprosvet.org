import Link from "next/link";

import styled from "@emotion/styled";
import { Stack } from "@mui/material";

import { useCheckout } from "./session";

const Item = styled.div`
  border: 1px solid var(--very-light-grey, #d9d9d9);
  display: flex;
  justify-content: space-between;
  padding: 1.2rem 1.5rem;
  width: 100%;
  box-shadow: none;
  box-sizing: border-box;
  &:first-child {
    border-radius: 0.25rem 0.25rem 0rem 0rem;
    border-bottom: 0;
  }
  &:last-child {
    border-radius: 0rem 0rem 0.25rem 0.25rem;
  }
`;

const Title = styled.span`
  width: 12.5rem;
  margin-right: 0.5rem;
  color: var(--light-grey, #979797);
  font-family: Montserrat;
  font-size: 0.875rem;
  font-style: normal;
  font-weight: 500;
  line-height: 1.125rem;
`;
const Value = styled.span`
  flex-grow: 1;
  color: var(--black, #000);
  font-family: Montserrat;
  font-size: 0.875rem;
  font-style: normal;
  font-weight: 500;
  line-height: 1.125rem;
`;
const Action = styled.span`
  color: var(--blue, #00aeef);
  font-family: Montserrat;
  font-size: 0.875rem;
  font-style: normal;
  font-weight: 500;
  line-height: 1.125rem;
  text-decoration: none;
`;

const UserContactLine = ({
  title,
  value,
}: {
  title: string;
  value?: string;
}) => {
  if (!value) {
    return null;
  }
  return (
    <Item>
      <Title>{title}</Title>
      <Value>{value}</Value>
      <Action>
        <Link href="/checkout">Изменить</Link>
      </Action>
    </Item>
  );
};

export const UserContactBlock = () => {
  const { email, country } = useCheckout();
  return (
    <Stack spacing={0} marginTop={"2rem"}>
      <UserContactLine title="Контактная информация" value={email} />
      <UserContactLine title="Страна" value={country?.label} />
    </Stack>
  );
};
