import { useEffect, useState } from "react";

import { VariantPrice } from "@/shop/product/model";
import styled from "@emotion/styled";

import { changeDeliveryMethod, useCheckout } from "../session";

type DeliveryOptionLineProps = {
  id: string;
  name: string;
  price: VariantPrice;
  selectedId?: string;
  onSelect: () => void;
};

type DeliveryOption = {
  id: string;
  type: "self-pickup" | "mail" | "inpost-paczkomat";
  name: string;
  rateProvider: {
    price: VariantPrice;
  };
};

const DeliveryOptionLine = ({
  id,
  name,
  price,
  selectedId,
  onSelect,
}: DeliveryOptionLineProps) => {
  const isSelected = selectedId === id;

  return (
    <Item isSelected={isSelected} onClick={onSelect}>
      <label>
        <Radio
          type="radio"
          name="deliveryOption"
          value={id}
          checked={isSelected}
          onChange={onSelect}
        />
        {name}
      </label>
      <Price>
        {price.amount}
        {price.currencyCode}
      </Price>
    </Item>
  );
};

const useDeliveryOptions = () => {
  const [deliveryOptions, setOption] = useState<DeliveryOption[]>([]);
  const { country } = useCheckout();

  useEffect(() => {
    if (country?.countryCode) {
      fetch(
        `/api/checkout/delivery-options?country_code=${country.countryCode}`,
      )
        .then(response => response.json())
        .then(res => setOption(res.deliveryMethods));
    }
  }, [country?.countryCode]);

  return deliveryOptions;
};

export const DeliveryOptions = () => {
  const deliveryOptions = useDeliveryOptions();
  const { selectedDeliveryMethod } = useCheckout();

  if (!Array.isArray(deliveryOptions)) {
    return null;
  }

  return (
    <Stack>
      {deliveryOptions.map(d => {
        return (
          <DeliveryOptionLine
            key={d.id}
            id={d.id}
            name={d.name}
            price={d.rateProvider.price}
            selectedId={selectedDeliveryMethod.id}
            onSelect={() => changeDeliveryMethod(d.id, d.type)}
          />
        );
      })}
    </Stack>
  );
};

const Item = styled.div`
  border: ${({ isSelected }: { isSelected: boolean }) =>
    isSelected
      ? "1px solid var(--blue, #00AEEF) !important"
      : "1px solid var(--very-light-grey, #D9D9D9)"};
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 1.2rem 1.5rem;
  width: 100%;
  box-shadow: none;
  box-sizing: border-box;
  &:first-child {
    border-radius: 0.25rem 0.25rem 0rem 0rem;
    border-bottom-color: transparent;
  }
  &:last-child {
    border-radius: 0rem 0rem 0.25rem 0.25rem;
    border-top-color: transparent;
  }
`;

const Price = styled.span`
  color: var(--black, #000);
  font-family: Montserrat;
  font-size: 0.875rem;
  font-style: normal;
  font-weight: 500;
  line-height: 1.125rem;
`;

const Stack = styled.div`
  margin-top: 2rem;
  cursor: pointer;
`;

const Radio = styled.input`
  margin-right: 0.5rem;
`;
