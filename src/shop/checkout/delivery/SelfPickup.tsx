import React from "react";

import styled from "@emotion/styled";

const TextContainer = styled.p`
  color: #000;
  font-family: Montserrat;
  font-size: 0.875rem;
  font-style: normal;
  font-weight: 400;
  line-height: 1.125rem;
  white-space: pre-line;
`;

export const SelfPickup = () => {
  return (
    <TextContainer>
      {
        "Самовывоз доступен в Варшаве и Вроцлаве.\nМы свяжемся с вами, чтобы согласовать время и адрес, где можно забрать ваш заказ."
      }
    </TextContainer>
  );
};
