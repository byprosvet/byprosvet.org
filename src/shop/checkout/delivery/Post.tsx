import { t } from "@/i18n";
import styled from "@emotion/styled";
import { Grid, TextField } from "@mui/material";

import { Title } from "../../cart/CartTable";
import {
  setPurchaseAddress,
  setPurchaseCity,
  setPurchaseFirstName,
  setPurchaseLastName,
  setPurchasePostalCode,
  useCheckout,
} from "../session";

const StyledTextField = styled(TextField)`
  width: 100%;
`;

export const Post = () => {
  const { firstName, lastName, address, postalCode, city } = useCheckout();

  return (
    <>
      <Title key="delivery-title">{t("checkout.delivery.post.address")}</Title>
      <Grid container spacing={2} key="delivery-name">
        <Grid item xs={12} sm={6}>
          <StyledTextField
            name="firstName"
            label={t("checkout.delivery.post.firstName")}
            value={firstName}
            onChange={e => setPurchaseFirstName(e.target.value)}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <StyledTextField
            name="lastName"
            label={t("checkout.delivery.post.lastName")}
            value={lastName}
            onChange={e => setPurchaseLastName(e.target.value)}
            required
          />
        </Grid>
      </Grid>
      <StyledTextField
        key="delivery-address"
        name="address"
        label={t("checkout.delivery.post.address")}
        value={address}
        onChange={e => setPurchaseAddress(e.target.value)}
        required
      />
      <Grid container spacing={2} key="delivery-city">
        <Grid item xs={12} sm={6}>
          <StyledTextField
            name="postalCode"
            label={t("checkout.delivery.post.zip")}
            value={postalCode}
            onChange={e => setPurchasePostalCode(e.target.value)}
            required
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <StyledTextField
            name="city"
            label={t("checkout.delivery.post.city")}
            value={city}
            onChange={e => setPurchaseCity(e.target.value)}
            required
          />
        </Grid>
      </Grid>
    </>
  );
};
