import { useState } from "react";

import styled from "@emotion/styled";
import { Grid, TextField } from "@mui/material";

import {
  setPaczkomatNumber,
  setPurchaseFirstName,
  setPurchaseLastName,
  setPurchasePhoneNumber,
  useCheckout,
} from "../session";

const StyledTextField = styled(TextField)`
  width: 100%;
`;

export const InPostPaczkomat = () => {
  const { firstName, lastName, phoneNumber, paczkomatNumber } = useCheckout();

  return (
    <>
      <Grid container spacing={2} key="delivery-name">
        <Grid item xs={12} sm={6}>
          <StyledTextField
            name="firstName"
            label="Імя"
            value={firstName}
            onChange={e => setPurchaseFirstName(e.target.value)}
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <StyledTextField
            name="lastName"
            label="Прозвішча"
            value={lastName}
            onChange={e => setPurchaseLastName(e.target.value)}
            required
          />
        </Grid>
      </Grid>
      <StyledTextField
        key="delivery-paczkomatnumber"
        label="Увядзіце, калі ласка, нумар пачкамата"
        value={paczkomatNumber || ""}
        onChange={e => setPaczkomatNumber(e.target.value)}
        name="numberPaczkomat"
      />
      <StyledTextField
        key="delivery-phone-number"
        label="Увядзіце, калі ласка, нумар тэлефона"
        value={phoneNumber || ""}
        onChange={e => setPurchasePhoneNumber(e.target.value)}
        name="phoneNumber"
      />
    </>
  );
};
