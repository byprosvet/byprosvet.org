"use client";

import { useRouter } from "next/navigation";

import { t } from "@/i18n";
import { ActionBottomButtons } from "@/uikit/actionBottomButtons";
import { Breadcrumbs } from "@/uikit/breadcrumbs";
import styled from "@emotion/styled";
import { Box } from "@mui/material";

import { FixedHeightContainer, Title } from "../cart/CartTable";
import { ProductList } from "../components/ProductList";
import { UserContactBlock } from "./UserContactBlock";
import { DeliveryOptions } from "./delivery/DeliveryOptions";
import { InPostPaczkomat } from "./delivery/InPost";
import { Post } from "./delivery/Post";
import { SelfPickup } from "./delivery/SelfPickup";
import { getCheckout, useCheckout } from "./session";

const startPurchase = async () => {
  const checkoutData = getCheckout();

  const { country, deliveryType, ...deliveryInfo } =
    checkoutData?.deliveryInfo ?? {};

  const checkoutDetails = {
    deliveryInfo: {
      ...deliveryInfo,
      country: country?.countryCode,
    },
    coupon: checkoutData.coupon,
    cart: checkoutData.items?.map(i => ({
      id: i.id,
      quantity: i.count,
    })),
  };

  const response = await fetch("/api/checkout", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(checkoutDetails),
  }).then(res => res.json());

  if (response.url) {
    location.href = response.url;
  }
};

export const Delivery = () => {
  const router = useRouter();
  const { selectedDeliveryMethod, email, country } = useCheckout();
  const toCart = () => {
    router.push("/cart");
  };

  return (
    <FullWidthContainer>
      <FixedHeightContainer padding={"2.5rem"}>
        <Breadcrumbs
          items={[
            { label: t("cart") },
            { label: t("checkout.info") },
            { label: t("checkout.delivery"), active: true },
            { label: t("checkout.payment") },
          ]}
        />
        <Box>
          <UserContactBlock />
          <Title>{t("checkout.delivery.method")}</Title>
          <DeliveryOptions />
          <Box m={2} />
          <FormWrapper>
            {selectedDeliveryMethod?.type === "self-pickup" && <SelfPickup />}
            {selectedDeliveryMethod?.type === "mail" && <Post />}
            {selectedDeliveryMethod?.type === "inpost-paczkomat" && (
              <InPostPaczkomat />
            )}
          </FormWrapper>
          <Box m={2} />
        </Box>
        <Box flexGrow={1}></Box>
        <ActionBottomButtons
          backLabel="Вярнуцца да рэдактавання інфармацыі"
          onBackClick={toCart}
          forwardLabel="Перайсці да аплаты"
          onForwardClick={() => startPurchase()}
          forwardDisabled={
            !email || !country?.countryCode || !selectedDeliveryMethod.id
          }
        />
      </FixedHeightContainer>

      <FixedHeightContainer>
        <ProductList />
      </FixedHeightContainer>
    </FullWidthContainer>
  );
};

const FullWidthContainer = styled.div`
  display: flex;
  justify-content: space-between;
  height: calc(100vh - 13.425rem);
  box-sizing: border-box;
`;

const FormWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  gap: 2rem;
`;
