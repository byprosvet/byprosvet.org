import { proxy, useSnapshot } from "valtio";

import type { CartItem } from "../cart/store";

export type ReadonlyDeep<T> = {
  readonly [P in keyof T]: ReadonlyDeep<T[P]>;
};

type Country = {
  countryCode: string;
  name: string;
  label: string;
};

export type DeliveryInfo = {
  deliveryMethodId: string;
  deliveryType: "self-pickup" | "mail" | "inpost-paczkomat";
  email: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  address: string;
  postalCode: string;
  city: string;
  country: Country | null;
  paczkomatNumber: string;
};

export type ActiveCheckout = {
  id: string;
  authToken: string;
  cartId: string;
  items: ReadonlyDeep<CartItem[]>;
  deliveryInfo?: Partial<DeliveryInfo>;
  coupon?: string;
};

export type CheckoutState =
  | ({
      isStarted: false;
    } & Partial<Record<keyof ActiveCheckout, undefined>>)
  | ({
      isStarted: true;
    } & ActiveCheckout);

const state = proxy<CheckoutState>({ isStarted: false });

export const getCheckout = () => ({ ...state });

export const startCheckout = (
  id: string,
  authToken: string,
  items: ReadonlyDeep<CartItem[]>,
) => {
  state.isStarted = true;
  state.id = id;
  state.authToken = authToken;
  state.items = [...items];
};

export const changeDeliveryMethod = (
  deliveryMethodId: string,
  deliveryType: DeliveryInfo["deliveryType"],
) => {
  state.deliveryInfo = {
    ...state.deliveryInfo,
    deliveryMethodId,
    deliveryType,
  };
};

export const applyCoupon = (coupon: string) => {
  state.coupon = coupon;
};

export const useCheckout = () => {
  const snapshot = useSnapshot(state);

  return {
    email: snapshot.deliveryInfo?.email,
    firstName: snapshot.deliveryInfo?.firstName,
    lastName: snapshot.deliveryInfo?.lastName,
    phoneNumber: snapshot.deliveryInfo?.phoneNumber,
    address: snapshot.deliveryInfo?.address,
    postalCode: snapshot.deliveryInfo?.postalCode,
    city: snapshot.deliveryInfo?.city,
    country: snapshot.deliveryInfo?.country,
    paczkomatNumber: snapshot.deliveryInfo?.paczkomatNumber,
    selectedDeliveryMethod: {
      id: snapshot.deliveryInfo?.deliveryMethodId,
      type: snapshot.deliveryInfo?.deliveryType,
    },
  };
};

export const setPurchaseEmail = (email: string) => {
  state.deliveryInfo = {
    ...state.deliveryInfo,
    email,
  };
};

export const setPurchaseFirstName = (firstName: string) => {
  state.deliveryInfo = {
    ...state.deliveryInfo,
    firstName,
  };
};

export const setPurchaseLastName = (lastName: string) => {
  state.deliveryInfo = {
    ...state.deliveryInfo,
    lastName,
  };
};

export const setPurchasePhoneNumber = (phoneNumber: string) => {
  state.deliveryInfo = {
    ...state.deliveryInfo,
    phoneNumber,
  };
};

export const setPurchaseAddress = (address: string) => {
  state.deliveryInfo = {
    ...state.deliveryInfo,
    address,
  };
};

export const setPurchasePostalCode = (postalCode: string) => {
  state.deliveryInfo = {
    ...state.deliveryInfo,
    postalCode,
  };
};

export const setPurchaseCity = (city: string) => {
  state.deliveryInfo = {
    ...state.deliveryInfo,
    city,
  };
};

export const setPurchaseCountry = (country: Country | null) => {
  state.deliveryInfo = {
    ...state.deliveryInfo,
    country,
  };
  state.deliveryInfo.deliveryMethodId = undefined;
  state.deliveryInfo.deliveryType = undefined;
};

export const setPaczkomatNumber = (paczkomatNumber: string) => {
  state.deliveryInfo = {
    ...state.deliveryInfo,
    paczkomatNumber,
  };
};
