"use client";

import { useEffect, useState } from "react";

import { useRouter } from "next/navigation";

import { t } from "@/i18n";
import { ActionBottomButtons } from "@/uikit/actionBottomButtons";
import { Breadcrumbs } from "@/uikit/breadcrumbs";
import Dropdown from "@/uikit/dropdown";
import TextInput from "@/uikit/input";
import styled from "@emotion/styled";
import { Box } from "@mui/material";

import { FixedHeightContainer, Title } from "../cart/CartTable";
import { ProductList } from "../components/ProductList";
import { setPurchaseCountry, setPurchaseEmail, useCheckout } from "./session";

type Country = {
  countryCode: string;
  name: string;
  label: string;
};

const useCountries = () => {
  const [countries, setCountries] = useState<Country[]>([
    { countryCode: "", name: "", label: "Страна не выбрана" },
  ]);
  useEffect(() => {
    fetch("/api/checkout/countries")
      .then(response => response.json())
      .then((res: { countries: Country[] }) => {
        setCountries([
          ...countries,
          ...res.countries
            .map((c: Country) => ({
              ...c,
              label: c.name,
            }))
            .sort((a, b) => a.label.localeCompare(b.label)),
        ]);
      });
  }, []);

  return countries;
};

export const Checkout = () => {
  const countries = useCountries();
  const { email, country } = useCheckout();
  const router = useRouter();
  const toCart = () => {
    router.push("/cart");
  };

  const toDelivery = () => {
    router.push("/checkout/delivery-options");
  };

  const validateEmail = (email: string) => {
    const re = /\S+@\S+\.\S+/;
    return re.test(email);
  };

  return (
    <FullWidthContainer>
      <FixedHeightContainer padding={"2.5rem"}>
        <Breadcrumbs
          items={[
            { label: t("cart") },
            { label: t("checkout.info"), active: true },
            { label: t("checkout.delivery") },
            { label: t("checkout.payment") },
          ]}
        />
        <Box boxSizing="border-box">
          <Title>{t("checkout.info.contact")}</Title>
          <TextInput
            label={t("checkout.info.email")}
            name="emailInput"
            onChange={e => setPurchaseEmail(e.target.value)}
            value={email ?? ""}
            validate={validateEmail}
          />
          <Box height="1rem" />
          <Dropdown
            label={t("checkout.info.country")}
            value={country?.countryCode || ""}
            options={countries.map(c => ({
              label: c.label,
              id: c.countryCode,
            }))}
            onChange={id => {
              setPurchaseCountry(
                countries.find(c => c.countryCode === id) as any,
              );
            }}
          />
        </Box>
        <Box flexGrow={1}></Box>
        <ActionBottomButtons
          backLabel={t("checkout.delivery.backToInfo")}
          onBackClick={toCart}
          forwardLabel={t("checkout.info.forwardToDelivery")}
          onForwardClick={toDelivery}
          forwardDisabled={!email || !country?.countryCode}
        />
      </FixedHeightContainer>

      <FixedHeightContainer>
        <ProductList />
      </FixedHeightContainer>
    </FullWidthContainer>
  );
};

const FullWidthContainer = styled.div`
  display: flex;
  justify-content: space-between;
  height: calc(100vh - 13.425rem);
  box-sizing: border-box;
`;
