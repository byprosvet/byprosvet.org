"use client";

import { Breadcrumbs, breadcrumbsWithHome } from "@/uikit/breadcrumbs";
import { containerPadding } from "@/uikit/layout";
import { Title } from "@/uikit/title";
import styled from "@emotion/styled";

import { DescriptionArea } from "./layout";
import type { Collection } from "./product";

type Props = {
  collection: Collection;
};

const breadcrumbItems = breadcrumbsWithHome;

export const CollectionDescription = ({ collection }: Props) => {
  return (
    <Container>
      <Breadcrumbs
        items={breadcrumbItems.concat([{ label: collection.title }])}
      />
      <DescriptionArea>
        <Title textAlign="left">{collection.title}</Title>
        <div
          dangerouslySetInnerHTML={{
            __html: collection.description,
          }}
        />
      </DescriptionArea>
    </Container>
  );
};

const Container = styled.div`
  display: block;
  width: 100%;
  ${containerPadding}
  background-color: #f8f7f5;
  padding-bottom: 2rem;
  padding-top: 0.05rem;
  margin-top: 0;
`;
