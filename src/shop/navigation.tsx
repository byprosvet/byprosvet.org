import { Suspense } from "react";

import Image from "next/image";
import Link from "next/link";

import { colors } from "@/uikit";
import {
  DesktopHide,
  MobileHide,
  landscapeMobilesAndUp,
  smallDesktopAndUp,
  tabletsAndUp,
} from "@/uikit/breakpoints";
import { Burger } from "@/uikit/burger";
import logoUrl from "@/uikit/logo.svg";
import styled from "@emotion/styled";

import { GlobalSearch } from "./GlobalSearch";
import { CartButton } from "./cart";

export const Header = () => {
  return (
    <>
      <TopBar key="topBar">
        <TopBarLeftContainer>
          <DesktopHide>
            <Burger />
          </DesktopHide>
          <Logo href="/">
            <Image src={logoUrl} alt="byprosvet logo" />
          </Logo>
        </TopBarLeftContainer>

        <Navigation>
          <Suspense fallback={null}>
            <GlobalSearch />
          </Suspense>

          <MobileHide>
            <SupportLink href="/donate">Падтрымаць нас</SupportLink>
          </MobileHide>

          <CartButton />
        </Navigation>
      </TopBar>
      <Spacer />
    </>
  );
};

const TopBarLeftContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

const TopBar = styled.header`
  display: flex;
  height: 50px;
  padding: 0 1rem 0 0;
  align-items: center;
  justify-content: space-between;
  position: fixed;
  background-color: ${colors.fontHighlight};
  width: 100vw;
  box-sizing: border-box;
  z-index: 1000;
  div:not(:has(input)) {
    transition:
      visibility 0s,
      width 0.1s;
  }
  &:has(input:focus) {
    div:not(:has(input)) {
      visibility: hidden;
      width: 0;
    }
  }
  ${tabletsAndUp`
  height: 60px;
  `}
  ${smallDesktopAndUp`
    height: 80px;
    padding-right: 2rem;
    `}
`;

const Logo = styled(Link)`
  display: block;
  width: 120px;
  padding: 1rem 2vw;
  img {
    height: 32px;
    width: auto;
  }

  ${landscapeMobilesAndUp`
      width: 180px;
      img {
        height: 48px;
        margin-left: 0.5rem;
      }
      `}
`;

const Navigation = styled.nav`
  font-size: 1rem;
  flex-direction: row;
  justify-content: flex-end;
  display: flex;
  align-items: center;
  gap: 0.5rem;
  ${landscapeMobilesAndUp`
        gap: 2rem;
        `}
`;

const SupportLink = styled(Link)`
  padding: 0.625rem 0.75rem;
  ${landscapeMobilesAndUp`
          padding: 0.625rem 1.5rem;
          `}
  background-color: ${colors.backgroundSecondary};
  border-radius: 4px;
  text-decoration: none;
  color: ${colors.fontHighlight};
  &:hover {
    background-color: ${colors.backgroundSecondaryHover};
  }
  &:active {
    border: 1px inset #fff;
  }
`;

export const Spacer = styled.div`
  width: 100%;
  height: 50px;
  ${tabletsAndUp`
            height: 60px;
            `}
  ${smallDesktopAndUp`
              height: 80px;
              `}
`;
