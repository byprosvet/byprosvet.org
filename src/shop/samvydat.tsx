"use client";

import { Fragment } from "react";

import Image from "next/image";

import { containerPadding } from "@/uikit/layout";
import styled from "@emotion/styled";

import { fontSecondary } from "../uikit/colors";
import { ArrowForward } from "../uikit/icons";
import volunteerHandUrl from "./img/volunteer-hand.png";
import type { Product } from "./product";
import { FeaturedPreview } from "./product/featured-preview";

export type SamvydatCollection = {
  title: string;
  description: string;
  products: Array<
    Product & {
      publishedAt?: {
        value: string;
      };
      newspaperUrl?: {
        value: string;
      };
    }
  >;
};

type Props = {
  collection: SamvydatCollection;
};

export const Samvydat = ({ collection }: Props) => {
  return (
    <Container>
      <PapersContainer>
        <h1>Штотыднёвы самвыдат</h1>
        <div
          dangerouslySetInnerHTML={{
            __html: collection.description,
          }}
        />
        <PapersList>
          {collection.products?.map(newspaper => (
            <Fragment key={newspaper.id}>
              <PaperContainer>
                <PaperPreview>
                  <FeaturedPreview
                    featuredImage={newspaper.featuredImage}
                    alt={newspaper.title}
                  />
                </PaperPreview>
                <h2>{newspaper.title}</h2>
                <p>
                  {newspaper.publishedAt
                    ? formatDate(newspaper.publishedAt.value)
                    : ""}
                </p>
                <PaperDescription
                  dangerouslySetInnerHTML={{
                    __html: newspaper.descriptionHtml,
                  }}
                />
              </PaperContainer>
              <DownloadButton link={newspaper.newspaperUrl?.value || ""} />
            </Fragment>
          ))}
        </PapersList>
      </PapersContainer>
      <FlyersContainer>
        <h1>Валанцёрскі рух</h1>
        <Link href="https://t.me/by_prosvet/2137" target="_blank">
          <span>Далучыцца</span>
          <ArrowForward />
        </Link>
        <HandContainer>
          <Image src={volunteerHandUrl} alt="Далучайся" />
        </HandContainer>
      </FlyersContainer>
    </Container>
  );
};

const formatDate = (date: string) => {
  const [year, month, day] = date.split("-");
  return `${day} ${getMonthName(+month)} ${year}`;
};

const monthes = [
  "студзеня",
  "лятага",
  "сакавіка",
  "красавіка",
  "траўня",
  "чэрвеня",
  "ліпеня",
  "жніўня",
  "верасня",
  "кастрычніка",
  "лістапада",
  "снежня",
];
const getMonthName = (month: number) => monthes[month - 1];

const Container = styled.div`
  display: flex;
  flex-direction: column;
  ${containerPadding}
  padding-top: 2.5rem;
  padding-bottom: 2.5rem;
  @media (min-width: 1201px) {
    flex-direction: row;
  }
  justify-content: center;
  background-color: #f8f7f5;
`;

const PapersContainer = styled.div`
  flex-grow: 2;
  @media (min-width: 641px) {
    padding: 0rem 3rem 0 0;
  }
  h1 {
    text-transform: uppercase;
  }
`;

const PapersList = styled.div`
  display: grid;
  grid-template-columns: 100%;
  grid-template-areas:
    "P1"
    "M1"
    "P2"
    "M2";
  @media (min-width: 641px) {
    grid-template-columns: 50% 50%;
    grid-template-areas:
      "P1 P2"
      "M1 M2";
  }
  justify-content: space-between;
`;
const PaperContainer = styled.div`
  &:first-of-type {
    grid-area: P1;
    margin: 0 2rem 0 0;
  }
  &:nth-of-type(2) {
    grid-area: P2;
  }
  h2 {
    font-size: 1.5rem;
    font-weight: 600;
    line-height: 29px;
    margin-bottom: 2px;
  }
  p {
    font-size: 12px;
    line-height: 15px;
    margin-top: 2px;
  }
`;
const PaperDescription = styled.div`
  line-height: 1.5;
  ul {
    padding: 0 0 0 20px;
  }
  li {
    margin: 0.5rem 0;
  }
  li::marker {
    font-size: 0.5rem;
    margin-right: 5rem;
  }
`;

const DownloadButton = (props: { link: string }) => (
  <Button href={props.link} target="_blank">
    <span>Больш падрабязна</span>
    <ArrowForward />
  </Button>
);
const Button = styled.a`
  &:first-of-type {
    grid-area: M1;
  }
  &:nth-of-type(2) {
    grid-area: M2;
  }
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin: 0 0 2rem 0;
  color: ${fontSecondary};
  background-color: transparent;
  border-radius: 4px;
  text-decoration: none;
  border: 2px solid ${fontSecondary};
  font-size: 1rem;
  @media (min-width: 641px) {
    width: 250px;
  }
  @media (min-width: 1201px) {
    margin: 0px;
    width: 300px;
  }
  svg {
    margin: 0.65rem 0 0.5rem 1vw;
    font-size: 16px;
  }
  span {
    margin: 0.5rem 0 0.5rem 0;
    font-size: 16px;
  }
`;
const PaperPreview = styled.div`
  width: 100%;
  height: 50vw;
  overflow: hidden;
  border: 1px solid #e2e2e2;
  img {
    max-width: 100%;
    height: auto;
  }
  @media (min-width: 641px) {
    width: 40vw;
    max-width: 380px;
    height: 240px;
  }
`;

const FlyersContainer = styled.div`
  flex-grow: 1;
  background-color: #00aeef;
  color: #fff;
  text-align: center;
  padding: 2rem 2rem 0 2rem;
  h1 {
    font-size: 40px;
    font-weight: 600;
  }
`;

const Link = styled.a`
  color: #fff;
  display: flex;
  flex-direction: row;
  justify-content: center;
  margin: 1rem 0;
  width: 100%;
  @media (min-width: 641px) {
    width: 300px;
    margin: 0 auto;
  }
  background-color: transparent;
  border-radius: 4px;
  text-decoration: none;
  border: 2px solid #fff;
  font-size: 1rem;
  svg {
    margin: 0.625rem 0 0.5rem 1vw;
    font-size: 16px;
  }
  span {
    margin: 0.5rem 0 0.5rem;
    font-size: 16px;
  }
`;
const HandContainer = styled.div`
  margin-top: 3rem;
  height: 340px;
  overflow: hidden;
`;
