import { ZodError } from "zod";

export const handleServerError =
  <T>(handler: (req: Request) => Promise<T>) =>
  async (req: Request) => {
    try {
      return await handler(req);
    } catch (e) {
      if (e instanceof ZodError) {
        return Response.json(
          {
            error: {
              message: "Validation has failed",
              e,
            },
          },
          { status: 400 },
        );
      }
      throw e;
    }
  };
