import { css } from "@emotion/react";

import { backgroundSecondary } from "./colors";

export const links = css`
  a {
    color: ${backgroundSecondary};
    &:hover {
      text-decoration: none;
    }
    &:active {
      text-decoration: none;
    }
  }
`;
