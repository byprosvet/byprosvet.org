import React, { useState } from "react";

import styled from "@emotion/styled";

const Label = styled.label`
  display: block;
  color: var(--dark-grey, #373737);
  font-family: Montserrat;
  font-size: 0.8125rem;
  font-weight: 400;
  line-height: 0.9375rem;
  margin-bottom: 0.25rem;
`;

const Input = styled.input`
  display: block;
  width: 100%;
  height: 2.5rem;
  padding: 0.5rem 1rem;
  font-family: Montserrat;
  font-size: 1rem;
  line-height: 1.25rem;
  border-radius: 0.25rem;
  border: 1px solid var(--very-light-grey, #d9d9d9);
  box-sizing: border-box;
  &:focus {
    border: 1px solid var(--dark-blue, #009dd7);
  }
`;

const Error = styled.span`
  color: red;
  font-size: 0.8rem;
`;

interface InputProps {
  label: string;
  name: string;
  value: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  validate?: (value: string) => boolean;
}

const TextInput: React.FC<InputProps> = ({
  label,
  name,
  value,
  onChange,
  validate,
}) => {
  const [error, setError] = useState("");

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (validate && !validate(event.target.value)) {
      setError("Неверный формат ввода");
    } else {
      setError("");
    }
    onChange(event);
  };

  return (
    <div>
      <Label htmlFor={name}>{label}</Label>
      <Input
        type="text"
        id={name}
        name={name}
        value={value}
        onChange={handleInputChange}
      />
      {error && <Error>{error}</Error>}
    </div>
  );
};

export default TextInput;
