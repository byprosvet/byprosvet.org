export const fontMain = "#000";

export const backgroundMain = "#000";

export const fontSecondary = "#00aeef";

export const backgroundSecondary = "#00aeef";

export const backgroundSecondaryHover = "#5ec7eb";

export const backgroundHighlight = "#f8f7f5";

export const fontHighlight = "#fff";
