import React from "react";

import styled from "@emotion/styled";

const Container = styled.div`
  width: 100%;
`;

const StyledLabel = styled.label`
  color: var(--dark-grey, #373737);
  font-family: Montserrat;
  font-size: 0.8125rem;
  font-style: normal;
  font-weight: 400;
  line-height: 0.9375rem;
  margin-bottom: 0.25rem;
  display: block;
`;

const StyledSelect = styled.select`
  width: 100%;
  height: 2.5rem;
  padding: 0.5rem 1rem;
  border-radius: 0.25rem;
  border: 1px solid var(--very-light-grey, #d9d9d9);
  appearance: none;
  background-color: white;
  font-family: inherit;
  &:focus {
    border: 1px solid var(--dark-blue, #009dd7);
    outline: none;
  }
`;

type OptionType = {
  id: string;
  label: string;
};

interface DropdownProps {
  label: string;
  options: OptionType[];
  value?: string;
  onChange: (value: string) => void;
}

export const Dropdown: React.FC<DropdownProps> = ({
  label,
  options,
  value,
  onChange,
}) => {
  const handleOptionChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    onChange(event.target.value);
  };

  return (
    <Container>
      <StyledLabel>{label}</StyledLabel>
      <StyledSelect value={value} onChange={handleOptionChange}>
        <option value="" disabled hidden>
          Выберите вариант...
        </option>
        {options.map(option => (
          <option key={option.id} value={option.id}>
            {option.label}
          </option>
        ))}
      </StyledSelect>
    </Container>
  );
};

export default Dropdown;
