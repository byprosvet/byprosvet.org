import { css } from "@emotion/react";

export const containerPadding = css`
  padding-left: 1.5rem;
  padding-right: 1.5rem;
  @media (min-width: 480px) {
    padding-left: 2.5rem;
    padding-right: 2.5rem;
  }
`;
