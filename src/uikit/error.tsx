"use client";

import Link from "next/link";

import { TextContainer } from "./text-container";

export const Error = () => (
  <TextContainer>
    <h1>Нажаль здарылася памылка :(</h1>
    <p>
      Такой старонкі не існуе або мы не можам яе паказаць. Паспрабуйце абнавіць
      старонку або <Link href="/">перайсці на галоўную</Link>
    </p>
  </TextContainer>
);
