import { css } from "@emotion/react";
import { CSSInterpolation } from "@emotion/serialize";
import styled from "@emotion/styled";

export const mediaMinWidth =
  (width: string) =>
  (rules: TemplateStringsArray, ...args: Array<CSSInterpolation>) => css`
    @media (min-width: ${width}) {
      ${css(rules, ...args)}
    }
  `;

export const mediaMaxWidth =
  (width: string) =>
  (rules: TemplateStringsArray, ...args: Array<CSSInterpolation>) => css`
    @media (max-width: ${width}) {
      ${css(rules, ...args)}
    }
  `;

export const mobileAndDown = mediaMaxWidth("480px");
export const tabletsAndUp = mediaMinWidth("481px");
export const landscapeMobilesAndUp = mediaMinWidth("641px");
export const smallDesktopAndUp = mediaMinWidth("768px");
export const desktopAndUp = mediaMinWidth("928px");

export const MobileHide = styled.div`
  display: none;
  ${desktopAndUp`
    display: inline-block;
  `}
`;

export const DesktopHide = styled.div`
  display: inline-block;
  ${desktopAndUp`
    display: none;
  `}
`;
