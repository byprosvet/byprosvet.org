"use client";

import { Fragment } from "react";

import Link from "next/link";

import { t } from "@/i18n";
import { ChevronRightIcon } from "@/uikit/icons";
import styled from "@emotion/styled";

import { smallDesktopAndUp, tabletsAndUp } from "./breakpoints";

export type BreadcrumbItem = {
  label: string;
  active?: boolean;
  href?: string;
};

type BreadcrumbsProps = {
  items: BreadcrumbItem[];
};

export const breadcrumbsWithHome: BreadcrumbItem[] = [
  { label: t("homepage"), href: "/" },
];

export const Breadcrumbs = ({ items }: BreadcrumbsProps) => {
  const activeIndex = items.findIndex(item => item.active);

  return (
    <BreadcrumbsContainer>
      {items.map((item, index) => (
        <Fragment key={index}>
          <BreadcrumbStep active={item.active} prev={index < activeIndex}>
            {item.href ? (
              <BreadcrumbsLink href={item.href}>{item.label}</BreadcrumbsLink>
            ) : (
              item.label
            )}
          </BreadcrumbStep>
          {index < items.length - 1 && <ChevronRightIcon color="disabled" />}
        </Fragment>
      ))}
    </BreadcrumbsContainer>
  );
};

const BreadcrumbsContainer = styled.div`
  grid-area: breadcrumbs;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  width: 100%;
  gap: 0.5rem;
  align-items: center;
  font-size: 0.75rem;
  margin: 0.5rem 0 0rem 0;
  ${tabletsAndUp`
    margin: 1.5rem 0 1rem 0;
  `}
  ${smallDesktopAndUp`
    font-size: 1rem;
    line-height: 1.5rem;
  `}
`;

export const BreadcrumbsLink = styled(Link)`
  text-decoration: none;
  &:active,
  &:visited {
    color: #000000;
  }
  &:hover {
    text-decoration: underline;
  }
`;

const BreadcrumbStep = styled.div<{ active?: boolean; prev?: boolean }>`
  color: ${props => (props.active ? "black" : props.prev ? "#00AEEF" : "grey")};
  font-size: 1rem;
  font-family: Montserrat;
  font-style: normal;
  font-weight: 500;
  line-height: 1.25rem;
  text-decoration: none;
`;
