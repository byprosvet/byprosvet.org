import { ReactNode, useState } from "react";

import styled from "@emotion/styled";

type SlidesProps = {
  currentSlide: number;
  slidesNumber: number;
};
export const Slider = (props: { children: ReactNode[] }) => {
  const slidesNumber = props.children.length;
  const [currentSlide, setSlide] = useState(0);
  const controls = [];
  for (let i = 0; i < slidesNumber; ++i) {
    controls.push(
      <SlideButton
        onClick={() => setSlide(i)}
        selected={i == currentSlide}
        key={i}
      />,
    );
  }
  return (
    <SliderContainer>
      <Slides currentSlide={currentSlide} slidesNumber={slidesNumber}>
        {props.children}
      </Slides>
      <SlidesControl>{controls}</SlidesControl>
    </SliderContainer>
  );
};

const SliderContainer = styled.section`
  width: 100vw;
  overflow: hidden;
`;
const Slides = styled.div`
  margin-left: ${(props: SlidesProps) => props.currentSlide * -100}vw;
  width: ${(props: SlidesProps) => props.slidesNumber * 100}vw;
  transition: margin-left 0.75s 0.2s;
  display: flex;
  flex-direction: row;
  background-color: #f2f2f2;
`;
export const Slide = styled.div`
  width: 100vw;
  display: flex;
  flex-direction: row;
`;
const SlidesControl = styled.div`
  position: relative;
  bottom: 4rem;
  display: flex;
  flex-direction: row;
  justify-content: center;
`;
const SlideButton = styled.button`
  border: 2px solid
    ${(props: { selected?: boolean }) => (props.selected ? "#000" : "#959595")};
  border-radius: 5px;
  width: 10px;
  height: 10px;
  background: ${(props: { selected?: boolean }) =>
    props.selected ? "#000" : "transparent"};
  padding: 0;
  margin: 0.75rem 1rem;
  cursor: pointer;
  &:active {
    border-color: #000;
  }
`;
