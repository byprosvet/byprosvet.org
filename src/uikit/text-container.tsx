"use client";

import styled from "@emotion/styled";

import { backgroundHighlight } from "./colors";
import { containerPadding } from "./layout";

export const TextContainer = styled.div`
  padding: 0.5rem 0;
  background-color: ${backgroundHighlight};
  ${containerPadding};
  line-height: 1.5;
  h1 {
    text-align: center;
    font-size: 1.75rem;
  }
  div {
    margin: 0 auto;
    max-width: 700px;
  }
  ul {
    padding-left: 0.5rem;
  }
  @media (min-width: 480px) {
    padding: 1rem 5rem;
    ul {
      padding-left: 1rem;
    }
  }
`;
