import isPropValid from "@emotion/is-prop-valid";
import styled from "@emotion/styled";

import {
  backgroundSecondary,
  backgroundSecondaryHover,
  fontHighlight,
} from "./colors";

type Props = {
  small?: boolean;
};

export const Button = styled("button", {
  shouldForwardProp: isPropValid,
})`
  display: flex;
  flex-direction: row;
  align-items: center;
  color: ${fontHighlight};
  background-color: ${backgroundSecondary};
  border-radius: 4px;
  border: 1px solid transparent;
  margin: 0.25rem 0;
  ${(props: Props) =>
    props.small
      ? "padding: 0.5rem 1rem 0.5rem 1rem;"
      : "padding: 0.75rem 1rem 0.75rem 1rem;"}
  font-size: 1rem;
  cursor: pointer;
  text-decoration: none;
  &:hover {
    background-color: ${backgroundSecondaryHover};
    cursor: pointer;
  }
  &:active {
    border: 1px inset #fff;
  }
  svg {
    display: inline-block;
    width: 1rem;
    height: 1rem;
    margin-right: 0.25rem;
  }
  span {
    display: inline-block;
    margin-left: 0.25rem;
  }
`;

export const ReverseButton = styled(Button)`
  color: ${backgroundSecondary};
  background-color: transparent;
  border: 1px solid ${backgroundSecondary};
  cursor: auto;
  &:hover {
    background-color: transparent;
    border-color: ${backgroundSecondaryHover};
    color: ${backgroundSecondaryHover};
  }
`;
