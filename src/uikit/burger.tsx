"use client";

import { useEffect, useState } from "react";

import { usePathname } from "next/navigation";

import { Global, css } from "@emotion/react";
import styled from "@emotion/styled";

import { backgroundMain, backgroundSecondary, fontHighlight } from "./colors";
import { MobileMenu } from "./mobileMenu";

export const Burger = () => {
  const path = usePathname();
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setOpen(false);
  }, [path]);

  return (
    <>
      {open && (
        <Global
          styles={css`
            body {
              overflow: hidden;
            }
          `}
        />
      )}
      <BurgerContainer>
        <BurgerIcon open={open} onClick={() => setOpen(!open)}>
          <span></span>
          <span></span>
          <span></span>
        </BurgerIcon>
        <Wrapper open={open}>
          <MobileMenu close={() => setOpen(false)} />
        </Wrapper>
      </BurgerContainer>
    </>
  );
};

const Wrapper = styled.div<{ open: boolean }>`
  z-index: 500;
  position: fixed;
  width: 100vw;
  height: 100%;
  left: -100%;
  top: 0;
  transition: 0.1s linear;
  background-color: ${fontHighlight};
  ${props => (props.open ? "left: 0;" : "")}
`;

const BurgerContainer = styled.div`
  width: 1.5rem;
  height: 1.5rem;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 1rem 0 1.5rem;
`;

const BurgerIcon = styled.div<{ open: boolean }>`
  z-index: 1000;
  width: 18px;
  height: 14px;
  position: relative;
  transform: rotate(0deg);
  transition: 0.5s ease-in-out;
  cursor: pointer;
  span {
    display: block;
    position: absolute;
    height: 2px;
    width: 100%;
    background: ${backgroundSecondary};
    opacity: 1;
    left: 0;
    transform: rotate(0deg);
    transition: 0.25s ease-in-out;
    &:nth-of-type(1) {
      top: 0px;
    }
    &:nth-of-type(2) {
      top: 5px;
    }
    &:nth-of-type(3) {
      top: 10px;
    }
    ${props =>
      props.open
        ? `
        background: ${backgroundMain};
    &:nth-of-type(1) {
      top: 5px;
      transform: rotate(135deg);
    }
    &:nth-of-type(2) {
      opacity: 0;
      left: -60px;
    }
    &:nth-of-type(3) {
      top: 5px;
      transform: rotate(-135deg);
    }`
        : ""}
  }
`;
