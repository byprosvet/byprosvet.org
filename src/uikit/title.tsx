import styled from "@emotion/styled";

type Props = {
  textAlign?: "center" | "left" | "right";
};

export const Title = styled.h1`
  ${(props: Props) =>
    props.textAlign === "center" ? `margin: 0 auto;` : "margin: 0;"}
  font-size: 1.5rem;
  ${(props: Props) =>
    props.textAlign ? `text-align: ${props.textAlign};` : ""}
  font-weight: 600;
`;
