import { FC } from "react";

import { ArrowBack, ArrowForward } from "@/uikit/icons";
import styled from "@emotion/styled";
import { Button } from "@mui/material";

import { backgroundSecondaryHover } from "./colors";

interface ActionBottomButtonsProps {
  backLabel: string;
  onBackClick: () => void;
  forwardLabel: string;
  onForwardClick: () => void;
  forwardDisabled?: boolean;
}

export const ActionBottomButtons: FC<ActionBottomButtonsProps> = ({
  backLabel,
  onBackClick,
  forwardLabel,
  onForwardClick,
  forwardDisabled = false,
}) => (
  <ButtonGroup>
    <OutlinedButton onClick={onBackClick} startIcon={<ArrowBack />}>
      {backLabel}
    </OutlinedButton>
    <ColoredButton
      onClick={onForwardClick}
      endIcon={<ArrowForward />}
      disabled={forwardDisabled}
    >
      {forwardLabel}
    </ColoredButton>
  </ButtonGroup>
);

const ButtonGroup = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  gap: 1rem;
  margin-top: 2rem;
  margin-bottom: 2rem;
`;

const StyledButton = styled(Button)`
  flex: 1 0 auto;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;

  @media (max-width: 600px) {
    flex: 1 0 100%;
  }
`;

const OutlinedButton = styled(StyledButton)`
  border: 1px solid var(--blue, #00aeef);
  color: var(--blue, #00aeef);
  display: flex;
  flex-wrap: nowrap;
`;

const ColoredButton = styled(StyledButton)`
  background-color: var(--blue, #00aeef);
  color: white;
  &:hover {
    background-color: ${backgroundSecondaryHover};
  }
`;
