import Link from "next/link";

import styled from "@emotion/styled";

import { backgroundSecondary, fontHighlight, fontMain } from "./colors";

type MobileMenuProps = {
  close: () => void;
};

export const MobileMenu = ({ close }: MobileMenuProps) => {
  return (
    <Container>
      <Top>
        <MenuLink href="/" onClick={close}>
          Галоўная
        </MenuLink>
        <MenuLink href="https://t.me/by_prosvet/2137" onClick={close}>
          Валанцёрскі рух
        </MenuLink>
      </Top>
      <Bottom>
        <SupportLinkMenu href="/donate" onClick={close}>
          Падтрымаць нас
        </SupportLinkMenu>
      </Bottom>
    </Container>
  );
};

const SupportLinkMenu = styled(Link)`
  width: 100%;
  height: 4rem;
  display: flex;
  box-sizing: border-box;
  padding: 0.625rem 0.75rem;
  align-items: center;
  justify-content: center;
  @media (min-width: 55rem) {
    padding: 0.625rem 1.5rem;
  }
  background-color: ${backgroundSecondary};
  border-radius: 4px;
  text-decoration: none;
  color: ${fontHighlight};
  font-size: 1rem;
  font-weight: 600;
`;

const MenuLink = styled(Link)`
  font-size: 1rem;
  line-height: 1.25rem;
  font-weight: 600;
  text-decoration: none;
  color: ${fontMain};
  &:hover {
    color: ${backgroundSecondary};
  }
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 4rem 1.5rem 1.5rem 1.5rem;
  height: 100%;
  box-sizing: border-box;
`;
const Top = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
  gap: 3rem;
`;

const Bottom = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
  gap: 1.5rem;
`;
