import AccessTime from "@mui/icons-material/AccessTime";
import AddIcon from "@mui/icons-material/Add";
import ArrowBack from "@mui/icons-material/ArrowBack";
import ArrowForward from "@mui/icons-material/ArrowForward";
import CheckIcon from "@mui/icons-material/Check";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import CreditCardIcon from "@mui/icons-material/CreditCard";
import DeleteIcon from "@mui/icons-material/Delete";
import DownloadIcon from "@mui/icons-material/Download";
import InstagramIcon from "@mui/icons-material/Instagram";
import MenuIcon from "@mui/icons-material/Menu";
import RemoveIcon from "@mui/icons-material/Remove";
import ShoppingCart from "@mui/icons-material/ShoppingCart";

export {
  AccessTime,
  ShoppingCart,
  ArrowForward,
  ArrowBack,
  DownloadIcon,
  CreditCardIcon,
  MenuIcon,
  InstagramIcon,
  AddIcon,
  RemoveIcon,
  CheckIcon,
  ChevronRightIcon,
  DeleteIcon,
};
