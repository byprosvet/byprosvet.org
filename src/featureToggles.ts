import { useEffect } from "react";

import { proxy, useSnapshot } from "valtio";

const featureToggles = proxy({
  isLoaded: false,
  toggles: {
    isCartEnabled: false,
  },
});

export const useFeatureToggles = () => {
  const snapshot = useSnapshot(featureToggles);
  useEffect(() => {
    if (snapshot.isLoaded) return;
    fetch("/api/feature-toggles")
      .then(res => res.json())
      .then(toggles => {
        featureToggles.isLoaded = true;
        featureToggles.toggles = toggles;
      });
  }, [snapshot.isLoaded]);
  return snapshot.toggles;
};
