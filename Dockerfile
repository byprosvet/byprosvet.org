FROM node:22-slim

COPY . /app
WORKDIR /app

EXPOSE 3000

CMD ["sh", "-c", "pnpm start"]

