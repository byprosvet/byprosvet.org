/** @type {import('next').NextConfig} */
export default {
  output: "standalone",
  images: {
    domains: ['cdn.shopify.com'],
    formats: ['image/avif', 'image/webp'],
  },
  compiler: {
    emotion: true
  }
};
